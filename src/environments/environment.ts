// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'carpooler-app',
    appId: '1:562043372308:web:641f89dccf136ec9437801',
    storageBucket: 'carpooler-app.appspot.com',
    apiKey: 'AIzaSyAlXC0MjBS4W82x1QCRJUGCfVA7WGahBFI',
    authDomain: 'carpooler-app.firebaseapp.com',
    messagingSenderId: '562043372308',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
