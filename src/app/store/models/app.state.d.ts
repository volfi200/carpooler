import { Conversation } from '../../shared/models/Conversation';
import { Currency } from '../../shared/models/Currency';
import { Friends } from '../../shared/models/Friends';
import { Message } from '../../shared/models/Message';
import { Request } from '../../shared/models/Request';
import { Ride } from '../../shared/models/Ride';
import { User } from '../../shared/models/User';

export interface AppState {
  readonly currentUser: User | null;
  readonly rides: Ride[];
  readonly loadedRide: Ride | null;
  readonly friends: Friends;
  readonly users: User[];
  readonly loadedUser: User | null;
  readonly conversations: Conversation[];
  readonly loadedConversation: Conversation | null;
  readonly messages: Message[];
  readonly currency: Currency;
  readonly requests: Request[];
}
