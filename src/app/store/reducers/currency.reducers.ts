import { createReducer, on } from '@ngrx/store';
import { Currency } from '../../shared/models/Currency';
import { logout } from '../actions/auth.actions';
import { loadCurrencySuccess } from '../actions/currency.actions';

export const initialState: Currency = {
  baseCode: 'EUR',
  rates: {},
};

export const currencyReducer = createReducer<Currency>(
  initialState,
  on(loadCurrencySuccess, (_, { currency }) => currency),
  on(logout, _ => initialState)
);
