import { createReducer, on } from '@ngrx/store';
import { Message } from '../../shared/models/Message';
import { logout } from '../actions/auth.actions';
import { loadMessagesSuccess } from '../actions/messages.actions';

export const initialState: Array<Message> = [];

export const messagesReducer = createReducer<Message[]>(
  initialState,
  on(loadMessagesSuccess, (_, { messages }) => messages),
  on(logout, _ => initialState)
);
