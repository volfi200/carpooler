import { createReducer, on } from '@ngrx/store';
import { Request } from '../../shared/models/Request';
import { logout } from '../actions/auth.actions';
import { loadRequestsSuccess } from '../actions/requests.actions';

export const initialListState: Array<Request> = [];

export const requestsReducer = createReducer<Request[]>(
  initialListState,
  on(loadRequestsSuccess, (_, { requests }) => requests),
  on(logout, _ => initialListState)
);
