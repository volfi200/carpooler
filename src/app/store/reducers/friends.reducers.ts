import { createReducer, on } from '@ngrx/store';
import { Friends } from '../../shared/models/Friends';
import { logout } from '../actions/auth.actions';
import {
  loadAcceptedFriendsSuccess,
  loadPendingFriendsSuccess,
  loadRequestedFriendsSuccess,
} from '../actions/friends.actions';

export const initialState: Friends = {
  accepted: [],
  requested: [],
  pending: [],
};

export const friendsReducer = createReducer<Friends>(
  initialState,
  on(loadAcceptedFriendsSuccess, (state, { accepted }) => {
    const newState: Friends = structuredClone(state);
    newState.accepted = accepted;
    return newState;
  }),
  on(loadPendingFriendsSuccess, (state, { pending }) => {
    const newState: Friends = structuredClone(state);
    newState.pending = pending;
    return newState;
  }),
  on(loadRequestedFriendsSuccess, (state, { requested }) => {
    const newState: Friends = structuredClone(state);
    newState.requested = requested;
    return newState;
  }),
  on(logout, _ => initialState)
);
