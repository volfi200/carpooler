import { createReducer, on } from '@ngrx/store';
import { User } from '../../shared/models/User';
import { loginSuccess, logout } from '../actions/auth.actions';

export const initialState = null;

export const currentUserReducer = createReducer<User | null>(
  initialState,
  on(loginSuccess, (_, { user }) => user),
  on(logout, _ => initialState)
);
