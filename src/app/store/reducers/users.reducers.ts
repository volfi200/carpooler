import { createReducer, on } from '@ngrx/store';
import { User } from '../../shared/models/User';
import { logout } from '../actions/auth.actions';
import { loadUsersSuccess, loadUserSuccess } from '../actions/user.actions';

export const initialListState: Array<User> = [];
export const initialState = null;

export const usersReducer = createReducer<User[]>(
  initialListState,
  on(loadUsersSuccess, (_, { users }) => users),
  on(logout, _ => initialListState)
);

export const loadedUserReducer = createReducer<User | null>(
  initialState,
  on(loadUserSuccess, (_, { user }) => user),
  on(logout, _ => initialState)
);
