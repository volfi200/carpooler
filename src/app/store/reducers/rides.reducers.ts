import { createReducer, on } from '@ngrx/store';
import { Ride } from '../../shared/models/Ride';
import { logout } from '../actions/auth.actions';
import {
  addPassengerSuccess,
  addPassengerToLoadedRide,
  createRideSuccess,
  deleteRideSuccess,
  loadRidesSuccess,
  removePassengerFromLoadedRide,
  removePassengerSuccess,
  setLoadedRideSuccess,
} from '../actions/ride.actions';

export const initialListState: Array<Ride> = [];
export const initialState = null;

export const ridesReducer = createReducer<Ride[]>(
  initialListState,
  on(loadRidesSuccess, (_, { rides }) => rides),
  on(createRideSuccess, (state, { ride, owner }) => {
    const newRide: Ride = structuredClone(ride);
    newRide.owner = owner;
    return [...state, newRide];
  }),
  on(deleteRideSuccess, (state, { rideId }) => state.filter(r => r.id !== rideId)),
  on(addPassengerSuccess, (state, { rideId, passengerId }) => {
    const newState: Ride[] = structuredClone(state);
    const ride = newState.find(r => r.id === rideId) as Ride;
    ride.passengerIds.push(passengerId);
    return newState;
  }),
  on(removePassengerSuccess, (state, { rideId, passengerId }) => {
    const newState: Ride[] = structuredClone(state);
    const ride = newState.find(r => r.id === rideId) as Ride;
    ride.passengerIds = ride?.passengerIds.filter(id => id !== passengerId);
    return newState;
  }),
  on(logout, _ => initialListState)
);

export const loadedRideReducer = createReducer<Ride | null>(
  initialState,
  on(setLoadedRideSuccess, (_, { ride }) => ride),
  on(addPassengerToLoadedRide, (state, { passengerId }) => {
    const newState: Ride = structuredClone(state);
    newState.passengerIds.push(passengerId);
    return newState;
  }),
  on(removePassengerFromLoadedRide, (state, { passengerId }) => {
    const newState: Ride = structuredClone(state);
    newState.passengerIds = newState.passengerIds.filter(id => id !== passengerId);
    return newState;
  }),
  on(logout, _ => initialState)
);
