import { createReducer, on } from '@ngrx/store';
import { Conversation } from '../../shared/models/Conversation';
import { logout } from '../actions/auth.actions';
import { loadConversationsListSuccess, setLoadedConversation } from '../actions/conversations.actions';

export const initialListState: Array<Conversation> = [];
export const initialState = null;

export const conversationsListReducer = createReducer<Conversation[]>(
  initialListState,
  on(loadConversationsListSuccess, (_, { conversations }) => conversations),
  on(logout, _ => initialListState)
);

export const loadedConversationReducer = createReducer<Conversation | null>(
  initialState,
  on(setLoadedConversation, (_, { conversation }) => conversation),
  on(logout, _ => initialState)
);
