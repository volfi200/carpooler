import { ActionReducerMap } from '@ngrx/store';
import { AppState } from '../models/app.state';
import { currentUserReducer } from './auth.reducers';
import { conversationsListReducer, loadedConversationReducer } from './conversations.reducers';
import { currencyReducer } from './currency.reducers';
import { friendsReducer } from './friends.reducers';
import { messagesReducer } from './messages.reducers';
import { requestsReducer } from './requests.reducers';
import { loadedRideReducer, ridesReducer } from './rides.reducers';
import { loadedUserReducer, usersReducer } from './users.reducers';

export const appReducers: ActionReducerMap<AppState> = {
  currentUser: currentUserReducer,
  rides: ridesReducer,
  loadedRide: loadedRideReducer,
  friends: friendsReducer,
  users: usersReducer,
  loadedUser: loadedUserReducer,
  conversations: conversationsListReducer,
  loadedConversation: loadedConversationReducer,
  messages: messagesReducer,
  currency: currencyReducer,
  requests: requestsReducer
};
