import { AppState } from '../models/app.state';

export const selectRequests = (state: AppState) => state.requests;

export const selectFirstRequest = (state: AppState) => state.requests[0];
