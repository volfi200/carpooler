import { createSelector } from '@ngrx/store';
import { Friends } from '../../shared/models/Friends';
import { AppState } from '../models/app.state';

export const selectFriends = createSelector(
  (state: AppState) => state.friends,
  (friends: Friends) => friends
);

export const selectAcceptedFriends = createSelector(
  (state: AppState) => state.friends,
  (friends: Friends) => friends.accepted
);

export const selectPendingFriends = createSelector(
  (state: AppState) => state.friends,
  (friends: Friends) => friends.pending
);

export const selectRequestedFriends = createSelector(
  (state: AppState) => state.friends,
  (friends: Friends) => friends.requested
);

export const selectAcceptedFriendIds = createSelector(
  (state: AppState) => state.friends,
  (friends: Friends) => friends.accepted.map(friend => friend.friendId)
);

export const selectPendingFriendIds = createSelector(
  (state: AppState) => state.friends,
  (friends: Friends) => friends.pending.map(friend => friend.friendId)
);

export const selectRequestedFriendIds = createSelector(
  (state: AppState) => state.friends,
  (friends: Friends) => friends.requested.map(friend => friend.friendId)
);
