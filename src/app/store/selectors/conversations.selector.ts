import { AppState } from '../models/app.state';

export const selectLoadedConversation = (state: AppState) => state.loadedConversation;

export const selectConversations = (state: AppState) => state.conversations;
