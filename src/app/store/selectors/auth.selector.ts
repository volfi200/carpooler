import { AppState } from '../models/app.state';

export const selectCurrentUser = (state: AppState) => state.currentUser;

export const selectCurrentUserLanguages = (state: AppState) => Object.keys(state.currentUser?.languages ?? {});
