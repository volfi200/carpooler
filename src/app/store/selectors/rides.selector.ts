import { AppState } from '../models/app.state';

export const selectLoadedRide = (state: AppState) => state.loadedRide;

export const selectRides = (state: AppState) => state.rides;
