import { AppState } from '../models/app.state';

export const selectCurrency = (state: AppState) => state.currency;

export const selectCurrencyBaseCode = (state: AppState) => state.currency.baseCode;
