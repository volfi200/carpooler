import { createAction, props } from '@ngrx/store';
import { User } from '../../shared/models/User';

export const loadUsersBySearchTerm = createAction(
  '[Users API] Load Users By Search Term',
  props<{ searchTerm: string }>()
);

export const loadAcceptedUsers = createAction('[Users API] Load Accepted Users');

export const loadPendingUsers = createAction('[Users API] Load Pending Users');

export const loadRequestedUsers = createAction('[Users API] Load Requested Users');

export const loadUsersSuccess = createAction('[Users API] Load Users Success', props<{ users: User[] }>());

// LOADED USER
export const loadUserById = createAction('[Users API] Load User By Id', props<{ id: string }>());

export const loadUserSuccess = createAction('[Users API] Load User Success', props<{ user: User }>());

export const loadUserFailed = createAction('[Users API] Load User Failed');
