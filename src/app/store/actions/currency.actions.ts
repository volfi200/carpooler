import { createAction, props } from '@ngrx/store';
import { Currency } from '../../shared/models/Currency';

export const loadCurrency = createAction('[Currency API] Load Currency', props<{ baseCode: string }>());

export const loadCurrencySuccess = createAction(
  '[Currency API] Load Currency Success',
  props<{ currency: Currency }>()
);

export const loadCurrencyFailed = createAction('[Currency API] Load Currency Failed');
