import { createAction, props } from '@ngrx/store';
import { User } from '../../shared/models/User';

export const loginWithEmail = createAction(
  '[Auth API] Login With Email',
  props<{ email: string; password: string }>()
);

export const loginWithFacebook = createAction('[Auth API] Login With Facebook');

export const checkIfAccountExists = createAction(
  '[Auth API] Check If Account Exists'
);

export const loginFailed = createAction('[Auth API] Login Failed');

export const loginSuccess = createAction(
  '[Auth API] Login Success',
  props<{ user: User }>()
);

export const logout = createAction('[Auth API] Logout');

export const initializeUser = createAction('[Auth API] Initialize User');
