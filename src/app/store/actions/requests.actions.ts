import { createAction, props } from '@ngrx/store';
import { Request } from '../../shared/models/Request';

export const createRequest = createAction(
  '[Request API] Create Request',
  props<{ departureFrom: string; arrivalTo: string; date: string }>()
);

export const createRequestSuccess = createAction(
  '[Request API] Create Request Success',
  props<{ requests: Request }>()
);

export const createRequestFailed = createAction('[Request API] Create Request Failed');

export const loadRequests = createAction(
  '[Request API] Load Requests',
  props<{ departureFrom: string; arrivalTo: string }>()
);

export const loadRequestsSuccess = createAction(
  '[Request API] Load Requests Success',
  props<{ requests: Request[] }>()
);

export const loadRequestsFailed = createAction('[Request API] Load Requests Failed');

export const loadRequestForDay = createAction(
  '[Request API] Load Request For Day',
  props<{ departureFrom: string; arrivalTo: string; day: string }>()
);
