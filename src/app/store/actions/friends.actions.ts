import { createAction, props } from '@ngrx/store';
import { Friendship } from '../../shared/models/Friendship';

export const loadAcceptedFriends = createAction('[Friends API] Load Accepted Friends');

export const loadAcceptedFriendsSuccess = createAction(
  '[Friends API] Load Accepted Friends Success',
  props<{ accepted: Friendship[] }>()
);

export const loadRequestedFriends = createAction('[Friends API] Load Requested Friends');

export const loadRequestedFriendsSuccess = createAction(
  '[Friends API] Load Requested Friends Success',
  props<{ requested: Friendship[] }>()
);

export const loadPendingFriends = createAction('[Friends API] Load Pending Friends');

export const loadPendingFriendsSuccess = createAction(
  '[Friends API] Load Pending Friends Success',
  props<{ pending: Friendship[] }>()
);

export const sendRequest = createAction('[Friends API] Send Friend Request', props<{ friendId: string }>());

export const removeFriendship = createAction('[Friends API] Remove Friendship', props<{ friendId: string }>());

export const removeInvitation = createAction('[Friends API] Remove Friend Invitation', props<{ friendId: string }>());

export const acceptRequest = createAction('[Friends API] Accept Friend Request', props<{ friendId: string }>());

export const declineRequest = createAction('[Friends API] Decline Friend Request', props<{ friendId: string }>());

export const friendActionSuccess = createAction('[Friends API] Friend Action Success');

export const friendActionFailed = createAction('[Friends API] Friend Action Failed');
