import { createAction, props } from '@ngrx/store';

export const createReview = createAction('[Review API] Create Review', props<{ rating: number; content: string }>());

export const createReviewSuccess = createAction('[Review API] Create Review Success');

export const createReviewFailed = createAction('[Review API] Create Review Failed');
