import { createAction, props } from '@ngrx/store';
import { Message } from '../../shared/models/Message';

export const loadMessages = createAction('[Messages API] Load Messages', props<{ conversationId: string }>());

export const loadMessagesSuccess = createAction(
  '[Messages API] Load Messages Success',
  props<{ messages: Message[] }>()
);

export const loadMessagesFailed = createAction('[Messages API] Load Messages Failed');

export const sendMessage = createAction('[Messages API] Send Message', props<{ content: string }>());

export const sendMessageSuccess = createAction('[Messages API] Send Message Success');

export const sendMessageFailed = createAction('[Messages API] Send Message Failed');
