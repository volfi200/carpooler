import { createAction, props } from '@ngrx/store';
import { Conversation } from '../../shared/models/Conversation';

export const loadConversationsList = createAction('[Conversations API] Load Conversations List');

export const loadConversationsListSuccess = createAction(
  '[Conversations API] Load Conversations List Success',
  props<{ conversations: Conversation[] }>()
);

export const findConversationWith = createAction(
  '[Conversations API] Find Conversation With',
  props<{ partnerId: string }>()
);

export const setLoadedConversation = createAction(
  '[Conversations API] Set Loaded Conversation',
  props<{ conversation: Conversation }>()
);

export const loadConversationsFailed = createAction('[Conversations API] Load Conversations Failed');
