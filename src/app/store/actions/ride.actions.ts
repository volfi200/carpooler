import { createAction, props } from '@ngrx/store';
import { Ride } from '../../shared/models/Ride';
import { User } from '../../shared/models/User';

export const createRide = createAction('[Ride API] Create Ride', props<{ ride: Ride; owner: User }>());

export const createRideSuccess = createAction('[Ride API] Create Ride Success', props<{ ride: Ride; owner: User }>());

export const createRideFailed = createAction('[Ride API] Create Ride Failed');

export const loadAllRides = createAction('[Ride API] Load All Rides');

export const loadRidesWherePassenger = createAction('[Ride API] Load Rides Where Passenger');

export const loadRidesWhereOwner = createAction('[Ride API] Load Rides Where Owner');

export const loadRidesSuccess = createAction('[Ride API] Load Rides Success', props<{ rides: Ride[] }>());

export const loadRidesFailed = createAction('[Ride API] Load Rides Failed');

export const addPassenger = createAction('[Ride API] Add Passenger', props<{ rideId: string; passengerId: string }>());

export const addPassengerSuccess = createAction(
  '[Ride API] Add Passenger Success',
  props<{ rideId: string; passengerId: string }>()
);

export const addPassengerFailed = createAction('[Ride API] Add Passenger Failed');

export const removePassenger = createAction(
  '[Ride API] Remove Passenger',
  props<{ rideId: string; passengerId: string }>()
);

export const removePassengerSuccess = createAction(
  '[Ride API] Remove Passenger Success',
  props<{ rideId: string; passengerId: string }>()
);

export const removePassengerFailed = createAction('[Ride API] Remove Passenger Failed');

export const deleteRide = createAction('[Ride API] Delete Ride', props<{ rideId: string }>());

export const deleteRideSuccess = createAction('[Ride API] Delete Ride Success', props<{ rideId: string }>());

export const deleteRideFailed = createAction('[Ride API] Delete Ride Failed');

export const setLoadedRide = createAction('[Ride API] Set Loaded Ride', props<{ ride: Ride }>());

export const setLoadedRideSuccess = createAction('[Ride API] Set Loaded Ride Success', props<{ ride: Ride }>());

// LOADED RIDES
export const addPassengerToLoadedRide = createAction(
  '[Ride API] Add Passenger To Loaded Ride',
  props<{ passengerId: string }>()
);

export const removePassengerFromLoadedRide = createAction(
  '[Ride API] Remove Passenger From Loaded Ride',
  props<{ passengerId: string }>()
);

export const deleteLoadedRide = createAction('[Ride API] Delete Loaded Ride');
