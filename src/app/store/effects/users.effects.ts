import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { catchError, EMPTY, forkJoin, from, map, of, switchMap, take, withLatestFrom } from 'rxjs';
import { User } from '../../shared/models/User';
import { ReviewService } from '../../shared/services/review.service';
import { UserService } from '../../shared/services/user.service';
import {
  loadAcceptedUsers,
  loadPendingUsers,
  loadRequestedUsers,
  loadUserById,
  loadUserFailed,
  loadUsersBySearchTerm,
  loadUsersSuccess,
  loadUserSuccess,
} from '../actions/user.actions';
import { AppState } from '../models/app.state';
import { selectAcceptedFriends, selectPendingFriends, selectRequestedFriends } from '../selectors/friends.selector';

@Injectable()
export class UsersEffects {
  loadUsersBySearchTerm$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadUsersBySearchTerm),
      switchMap(({ searchTerm }) =>
        from(this.userService.getByName(searchTerm)).pipe(
          catchError(() => EMPTY),
          map(users => loadUsersSuccess({ users }))
        )
      )
    )
  );

  loadAcceptedUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadAcceptedUsers),
      withLatestFrom(this.store.pipe(select(selectAcceptedFriends))),
      switchMap(([, accepted]) => {
        if (!!accepted.length) {
          const getUserByIdRequests = accepted.map(({ friendId }) => this.userService.getById(friendId).pipe(take(1)));
          return forkJoin(getUserByIdRequests).pipe(
            catchError(() => EMPTY),
            map(users => loadUsersSuccess({ users }))
          );
        } else {
          return of(loadUsersSuccess({ users: [] }));
        }
      })
    )
  );

  loadRequestedUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadRequestedUsers),
      withLatestFrom(this.store.pipe(select(selectRequestedFriends))),
      switchMap(([, requested]) => {
        if (!!requested.length) {
          const getUserByIdRequests = requested.map(({ friendId }) => this.userService.getById(friendId).pipe(take(1)));
          return forkJoin(getUserByIdRequests).pipe(
            catchError(() => EMPTY),
            map(users => loadUsersSuccess({ users }))
          );
        } else {
          return of(loadUsersSuccess({ users: [] }));
        }
      })
    )
  );

  loadPendingUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadPendingUsers),
      withLatestFrom(this.store.pipe(select(selectPendingFriends))),
      switchMap(([, pending]) => {
        if (!!pending.length) {
          const getUserByIdRequests = pending.map(({ friendId }) => this.userService.getById(friendId).pipe(take(1)));
          return forkJoin(getUserByIdRequests).pipe(
            catchError(() => EMPTY),
            map(users => loadUsersSuccess({ users }))
          );
        } else {
          return of(loadUsersSuccess({ users: [] }));
        }
      })
    )
  );

  loadUserById$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadUserById),
      switchMap(({ id }) =>
        this.userService.getById(id).pipe(
          switchMap(user => {
            return this.reviewService.getReviewsForUserId(user.id as string).pipe(
              switchMap(reviews => {
                if (reviews.length) {
                  const getAuthorRequests = reviews.map(({ authorId }) => this.userService.getById(authorId));
                  return forkJoin(getAuthorRequests).pipe(
                    map(authors => {
                      return reviews.map(review => {
                        const author = authors.find(author => author.id === review.authorId);
                        return { ...review, author };
                      });
                    })
                  );
                }
                return of(reviews);
              }),
              map(reviewsWithAuthors => {
                const userCopy: User = structuredClone(user);
                userCopy.reviews = reviewsWithAuthors;
                return loadUserSuccess({ user: userCopy });
              })
            );
          }),
          catchError(error => {
            console.error(error);
            return of(loadUserFailed());
          })
        )
      )
    )
  );

  constructor(
    private readonly actions$: Actions,
    private readonly userService: UserService,
    private readonly reviewService: ReviewService,
    private readonly store: Store<AppState>
  ) {}
}
