import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { EMPTY, Observable, catchError, forkJoin, from, map, of, switchMap, take, tap, withLatestFrom } from 'rxjs';
import { Conversation } from '../../shared/models/Conversation';
import { ConversationService } from '../../shared/services/conversation.service';
import { UserService } from '../../shared/services/user.service';
import {
  findConversationWith,
  loadConversationsFailed,
  loadConversationsList,
  loadConversationsListSuccess,
  setLoadedConversation,
} from '../actions/conversations.actions';
import { loadMessages } from '../actions/messages.actions';
import { AppState } from '../models/app.state';
import { selectCurrentUser } from '../selectors/auth.selector';

@Injectable()
export class ConversationsEffects {
  loadConversationsList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadConversationsList),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      switchMap(([, currentUser]) =>
        this.conversationService.getConversations(currentUser?.id as string).pipe(
          switchMap(conversations =>
            forkJoin(
              conversations.map(conversation => this.mapNameToConversation(currentUser?.id as string, conversation))
            ).pipe(
              map(conversationsWithNames => loadConversationsListSuccess({ conversations: conversationsWithNames }))
            )
          ),
          catchError(error => {
            return of(loadConversationsFailed());
          })
        )
      )
    )
  );

  findConversationWith$ = createEffect(() =>
    this.actions$.pipe(
      ofType(findConversationWith),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      switchMap(([{ partnerId }, currentUser]) =>
        this.conversationService.getConversation(currentUser?.id as string, partnerId).pipe(
          switchMap(conversation => {
            if (conversation) {
              return this.mapNameToConversation(currentUser?.id as string, conversation).pipe(
                map(conversationWithName => {
                  return setLoadedConversation({ conversation: conversationWithName });
                })
              );
            } else {
              const conversation: Conversation = {
                users: { [currentUser?.id as string]: true, [partnerId]: true },
                lastMessage: null,
              };
              return from(this.conversationService.createConversation(conversation)).pipe(
                switchMap(() => {
                  return this.mapNameToConversation(currentUser?.id as string, conversation).pipe(
                    map(conversationWithName => {
                      return setLoadedConversation({ conversation: conversationWithName });
                    })
                  );
                }),
                catchError(error => {
                  console.error(error);
                  return of(loadConversationsFailed());
                })
              );
            }
          })
        )
      )
    )
  );

  setLoadedConversation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setLoadedConversation),
      map(({ conversation }) => loadMessages({ conversationId: conversation.id as string }))
    )
  );

  loadConversationsFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loadConversationsFailed),
        tap(async () => {
          const toast = await this.toastController.create({
            message: 'Loading conversations has failed.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  private mapNameToConversation(currentUserId: string, conversation: Conversation): Observable<Conversation> {
    let otherUserId = '';
    for (const userId in conversation.users) {
      if (userId !== currentUserId) {
        otherUserId = userId;
        break;
      }
    }
    return this.userService.getById(otherUserId).pipe(
      take(1),
      map(({ name }) => ({
        ...conversation,
        partnerName: name,
      })),
      catchError(() => EMPTY)
    );
  }

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<AppState>,
    private readonly conversationService: ConversationService,
    private readonly userService: UserService,
    private readonly toastController: ToastController
  ) {}
}
