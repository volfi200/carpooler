import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { catchError, from, map, of, switchMap, tap, withLatestFrom } from 'rxjs';
import { Request } from '../../shared/models/Request';
import { RequestService } from '../../shared/services/request.service';
import {
  createRequest,
  createRequestFailed,
  loadRequestForDay,
  loadRequests,
  loadRequestsFailed,
  loadRequestsSuccess,
} from '../actions/requests.actions';
import { AppState } from '../models/app.state';
import { selectCurrentUser } from '../selectors/auth.selector';
import { selectRequests } from '../selectors/requests.selector';

@Injectable()
export class RequestsEffects {
  createRequest$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(createRequest),
        withLatestFrom(this.store.pipe(select(selectRequests)), this.store.pipe(select(selectCurrentUser))),
        switchMap(([{ departureFrom, arrivalTo, date }, requests, currentUser]) => {
          const requestIdIfExists = requests.find(
            request =>
              request.departureFrom === departureFrom &&
              request.arrivalTo === arrivalTo &&
              request.dayInMillis === new Date(date).getTime()
          )?.id;
          if (requestIdIfExists) {
            return from(this.requestService.addUserToRequest(requestIdIfExists, currentUser?.id as string));
          }
          const request: Request = {
            departureFrom,
            arrivalTo,
            dayInMillis: new Date(date).getTime(),
            users: { [currentUser?.id as string]: true },
          };
          return from(this.requestService.createRequest(request));
        }),
        catchError(error => {
          console.error(error);
          return of(createRequestFailed());
        })
      ),
    { dispatch: false }
  );

  loadRequests$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadRequests),
      switchMap(({ departureFrom, arrivalTo }) =>
        this.requestService.getRequests(departureFrom, arrivalTo).pipe(
          map(requests => {
            const requestsWithCount: Request[] = requests.map(
              request => <Request>{ ...request, userCount: Object.keys(request.users).length }
            );
            return loadRequestsSuccess({ requests: requestsWithCount });
          }),
          catchError(error => {
            console.error(error);
            return of(loadRequestsFailed);
          })
        )
      )
    )
  );

  loadRequestsFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loadRequestsFailed),
        tap(async () => {
          const toast = await this.toastController.create({
            message: 'Loading requests has failed.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  loadRequestForDay$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadRequestForDay),
      switchMap(({ departureFrom, arrivalTo, day }) =>
        this.requestService.getRequestForDay(departureFrom, arrivalTo, day).pipe(
          map(requests => {
            const requestsWithCount: Request[] = requests.map(
              request => <Request>{ ...request, userCount: Object.keys(request.users).length }
            );
            return loadRequestsSuccess({ requests: requestsWithCount });
          }),
          catchError(error => {
            console.error(error);
            return of(loadRequestsFailed);
          })
        )
      )
    )
  );

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<AppState>,
    private readonly toastController: ToastController,
    private readonly requestService: RequestService
  ) {}
}
