import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { catchError, from, map, mergeMap, of, switchMap, tap, withLatestFrom } from 'rxjs';
import { Message } from '../../shared/models/Message';
import { ConversationService } from '../../shared/services/conversation.service';
import { MessageService } from '../../shared/services/message.service';
import {
  loadMessages,
  loadMessagesFailed,
  loadMessagesSuccess,
  sendMessage,
  sendMessageFailed,
  sendMessageSuccess,
} from '../actions/messages.actions';
import { AppState } from '../models/app.state';
import { selectCurrentUser } from '../selectors/auth.selector';
import { selectLoadedConversation } from '../selectors/conversations.selector';

@Injectable()
export class MessagesEffects {
  loadMessages$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadMessages),
      switchMap(({ conversationId }) =>
        from(this.messageService.getMessages(conversationId)).pipe(
          map(messages => loadMessagesSuccess({ messages })),
          catchError(error => {
            console.error(error);
            return of(loadMessagesFailed());
          })
        )
      )
    )
  );

  loadMessagesFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loadMessagesFailed),
        tap(async () => {
          const toast = await this.toastController.create({
            message: 'Loading Messages has failed.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  sendMessage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(sendMessage),
      withLatestFrom(this.store.pipe(select(selectCurrentUser)), this.store.pipe(select(selectLoadedConversation))),
      mergeMap(([{ content }, user, conversation]) => {
        const message: Message = {
          content,
          conversationId: conversation?.id,
          userId: user?.id as string,
          date: Date.now(),
        };
        return from(this.messageService.createMessage(message)).pipe(
          mergeMap(() => from(this.conversationService.updateLastMessage(conversation?.id as string, message))),
          map(() => sendMessageSuccess()),
          catchError(error => {
            console.error(error);
            return of(sendMessageFailed());
          })
        );
      })
    )
  );

  sendMessageFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(sendMessageFailed),
        tap(async () => {
          const toast = await this.toastController.create({
            message: 'Sending message has failed.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<AppState>,
    private readonly messageService: MessageService,
    private readonly conversationService: ConversationService,
    private readonly toastController: ToastController
  ) {}
}
