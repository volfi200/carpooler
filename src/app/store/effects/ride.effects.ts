import { Injectable } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { EMPTY, Observable, catchError, forkJoin, from, map, of, switchMap, tap, withLatestFrom } from 'rxjs';
import { Ride } from '../../shared/models/Ride';
import { RideService } from '../../shared/services/ride.service';
import { UserService } from '../../shared/services/user.service';
import {
  addPassenger,
  addPassengerFailed,
  addPassengerSuccess,
  addPassengerToLoadedRide,
  createRide,
  createRideFailed,
  createRideSuccess,
  deleteLoadedRide,
  deleteRide,
  deleteRideFailed,
  deleteRideSuccess,
  loadAllRides,
  loadRidesFailed,
  loadRidesSuccess,
  loadRidesWhereOwner,
  loadRidesWherePassenger,
  removePassenger,
  removePassengerFailed,
  removePassengerFromLoadedRide,
  removePassengerSuccess,
  setLoadedRide,
  setLoadedRideSuccess,
} from '../actions/ride.actions';
import { AppState } from '../models/app.state';
import { selectCurrentUser } from '../selectors/auth.selector';
import { selectLoadedRide } from '../selectors/rides.selector';

@Injectable()
export class RideEffects {
  private loading: HTMLIonLoadingElement | undefined;

  createRide$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createRide),
      tap(async () => await this.loading?.present()),
      switchMap(({ ride, owner }) => {
        return from(this.rideService.createRide(ride)).pipe(
          map(() => createRideSuccess({ ride, owner })),
          catchError(error => {
            console.error(error);
            return of(createRideFailed());
          })
        );
      })
    )
  );

  createRideFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(createRideFailed),
        tap(async () => {
          await this.loading?.dismiss();
          const toast = await this.toastController.create({
            message: 'Ride creation has failed.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  createRideSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(createRideSuccess),
        tap(async () => {
          await this.loading?.dismiss();
          const toast = await this.toastController.create({
            message: 'Ride creation was successful.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  deleteRide$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteRide),
      switchMap(({ rideId }) => {
        return from(this.rideService.deleteRide(rideId)).pipe(
          map(() => deleteRideSuccess({ rideId })),
          catchError(error => {
            console.error(error);
            return of(deleteRideFailed());
          })
        );
      })
    )
  );

  deleteRideSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteRideSuccess),
        tap(async () => {
          await this.loading?.dismiss();
          const toast = await this.toastController.create({
            message: 'The ride has been successfully deleted.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  removePassenger$ = createEffect(() =>
    this.actions$.pipe(
      ofType(removePassenger),
      switchMap(({ rideId, passengerId }) =>
        from(this.rideService.removePassenger(rideId, passengerId)).pipe(
          map(() => removePassengerSuccess({ rideId, passengerId })),
          catchError(error => {
            console.error(error);
            return of(removePassengerFailed());
          })
        )
      )
    )
  );

  removePassengerSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(removePassengerSuccess),
        tap(async () => {
          await this.loading?.dismiss();
          const toast = await this.toastController.create({
            message: 'Booking has been successfully deleted.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  removePassengerFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(removePassengerFailed),
        tap(async () => {
          await this.loading?.dismiss();
          const toast = await this.toastController.create({
            message: 'Booking has failed to be deleted.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  addPassenger$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addPassenger),
      switchMap(({ rideId, passengerId }) =>
        from(this.rideService.addPassenger(rideId, passengerId)).pipe(
          map(() => addPassengerSuccess({ rideId, passengerId })),
          catchError(error => {
            console.error(error);
            return of(addPassengerFailed());
          })
        )
      )
    )
  );

  addPassengerSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addPassengerSuccess),
        tap(async () => {
          await this.loading?.dismiss();
          const toast = await this.toastController.create({
            message: 'Booking has been successfully created.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  addPassengerFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addPassengerFailed),
        tap(async () => {
          await this.loading?.dismiss();
          const toast = await this.toastController.create({
            message: 'Booking has failed to be created.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  loadAllRides$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadAllRides),
      switchMap(() =>
        this.rideService.getAll().pipe(
          switchMap(rides => {
            if (rides.length) {
              return forkJoin(rides.map(ride => this.mapOwnerToRide(ride))).pipe(
                map(ridesWithOwners => loadRidesSuccess({ rides: ridesWithOwners }))
              );
            } else {
              return of(loadRidesSuccess({ rides: [] }));
            }
          }),
          catchError(() => of(loadRidesFailed()))
        )
      )
    )
  );

  loadRidesWhereOwner$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadRidesWhereOwner),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      switchMap(([, user]) =>
        this.rideService.getWhereOwner(user?.id as string).pipe(
          switchMap(rides => {
            if (rides.length) {
              return forkJoin(rides.map(ride => this.mapOwnerToRide(ride))).pipe(
                map(ridesWithOwners => loadRidesSuccess({ rides: ridesWithOwners }))
              );
            } else {
              return of(loadRidesSuccess({ rides: [] }));
            }
          }),
          catchError(() => of(loadRidesFailed()))
        )
      )
    )
  );

  loadRidesWherePassenger$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadRidesWherePassenger),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      switchMap(([, user]) =>
        this.rideService.getWherePassenger(user?.id as string).pipe(
          switchMap(rides => {
            if (rides.length) {
              return forkJoin(rides.map(ride => this.mapOwnerToRide(ride))).pipe(
                map(ridesWithOwners => loadRidesSuccess({ rides: ridesWithOwners }))
              );
            } else {
              return of(loadRidesSuccess({ rides: [] }));
            }
          }),
          catchError(() => of(loadRidesFailed()))
        )
      )
    )
  );

  loadRidesFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loadRidesFailed),
        tap(async () => {
          const toast = await this.toastController.create({
            message: 'Loading rides has failed.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  setLoadedRide$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setLoadedRide),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      switchMap(([{ ride }, currentUser]) => {
        if (ride.ownerId === currentUser?.id && ride.passengerIds.length) {
          return forkJoin(ride.passengerIds.map(id => this.userService.getById(id))).pipe(
            map(users => {
              const rideCopy: Ride = structuredClone(ride);
              rideCopy.passengers = users;
              return setLoadedRideSuccess({ ride: rideCopy });
            })
          );
        }
        return of(setLoadedRideSuccess({ ride }));
      })
    )
  );

  addPassengerToLoadedRide$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addPassengerToLoadedRide),
      withLatestFrom(this.store.pipe(select(selectLoadedRide))),
      map(([{ passengerId }, loadedRide]) => addPassenger({ rideId: loadedRide?.id as string, passengerId }))
    )
  );

  removePassengerFromLoadedRide$ = createEffect(() =>
    this.actions$.pipe(
      ofType(removePassengerFromLoadedRide),
      withLatestFrom(this.store.pipe(select(selectLoadedRide))),
      map(([{ passengerId }, loadedRide]) => removePassenger({ rideId: loadedRide?.id as string, passengerId }))
    )
  );

  deleteLoadedRide$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteLoadedRide),
      withLatestFrom(this.store.pipe(select(selectLoadedRide))),
      map(([, loadedRide]) => deleteRide({ rideId: loadedRide?.id as string })),
      tap(() => this.navController.navigateRoot('/home/browse'))
    )
  );

  private async initializeLoading() {
    this.loading = await this.loadingController.create({
      message: 'Loading ...',
    });
  }

  private mapOwnerToRide(ride: Ride): Observable<Ride> {
    return this.userService.getById(ride.ownerId as string).pipe(
      map(owner => {
        const rideCopy: Ride = structuredClone(ride);
        rideCopy.owner = owner;
        return rideCopy;
      }),
      catchError(() => EMPTY)
    );
  }

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<AppState>,
    private readonly toastController: ToastController,
    private readonly rideService: RideService,
    private readonly userService: UserService,
    private readonly loadingController: LoadingController,
    private readonly navController: NavController
  ) {
    this.initializeLoading();
  }
}
