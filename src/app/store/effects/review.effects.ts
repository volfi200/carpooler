import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { catchError, from, map, mergeMap, of, tap, withLatestFrom } from 'rxjs';
import { Review } from '../../shared/models/Review';
import { ReviewService } from '../../shared/services/review.service';
import { loadCurrencyFailed } from '../actions/currency.actions';
import { createReview, createReviewFailed, createReviewSuccess } from '../actions/review.actions';
import { AppState } from '../models/app.state';
import { selectCurrentUser } from '../selectors/auth.selector';
import { selectLoadedUser } from '../selectors/users.selector';

@Injectable()
export class ReviewEffects {
  createReview$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createReview),
      withLatestFrom(this.store.pipe(select(selectCurrentUser)), this.store.pipe(select(selectLoadedUser))),
      mergeMap(([{ rating, content }, currentUser, loadedUser]) => {
        const review: Review = {
          authorId: currentUser?.id as string,
          targetId: loadedUser?.id as string,
          rating,
          content,
          date: Date.now(),
        };
        return from(this.reviewService.createReview(review)).pipe(
          map(() => createReviewSuccess()),
          catchError(() => of(createReviewFailed()))
        );
      })
    )
  );

  createReviewFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loadCurrencyFailed),
        tap(async () => {
          const toast = await this.toastController.create({
            message: 'Creating review has failed.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<AppState>,
    private readonly toastController: ToastController,
    private readonly reviewService: ReviewService
  ) {}
}
