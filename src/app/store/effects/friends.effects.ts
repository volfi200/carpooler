import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { catchError, from, map, of, switchMap, withLatestFrom } from 'rxjs';
import { FriendshipState } from '../../shared/models/FriendshipState';
import { FriendsService } from '../../shared/services/friends.service';
import {
  acceptRequest,
  declineRequest,
  friendActionFailed,
  friendActionSuccess,
  loadAcceptedFriends,
  loadAcceptedFriendsSuccess,
  loadPendingFriends,
  loadPendingFriendsSuccess,
  loadRequestedFriends,
  loadRequestedFriendsSuccess,
  removeFriendship,
  removeInvitation,
  sendRequest,
} from '../actions/friends.actions';
import { AppState } from '../models/app.state';
import { selectCurrentUser } from '../selectors/auth.selector';
import { selectFriends } from '../selectors/friends.selector';

@Injectable()
export class FriendsEffects {
  sendRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(sendRequest),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      switchMap(([{ friendId }, user]) => {
        const friendship = { friendId, state: FriendshipState.PENDING };
        return from(this.friendsService.sendRequest(friendship, user?.id as string)).pipe(
          map(() => friendActionSuccess()),
          catchError(error => {
            console.error(error);
            return of(friendActionFailed());
          })
        );
      })
    )
  );

  removeInvitation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(removeInvitation),
      withLatestFrom(this.store.pipe(select(selectFriends))),
      switchMap(([{ friendId }, { requested }]) => {
        const friendshipId = requested.find(fs => fs.friendId === friendId)?.id as string;
        return from(this.friendsService.removeFriendship(friendshipId)).pipe(
          map(() => friendActionSuccess()),
          catchError(error => {
            console.error(error);
            return of(friendActionFailed());
          })
        );
      })
    )
  );

  acceptRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(acceptRequest),
      withLatestFrom(this.store.pipe(select(selectFriends))),
      switchMap(([{ friendId }, { pending }]) => {
        const friendshipId = pending.find(fs => fs.friendId === friendId)?.id as string;
        return from(this.friendsService.acceptRequest(friendshipId)).pipe(
          map(() => friendActionSuccess()),
          catchError(error => {
            console.error(error);
            return of(friendActionFailed());
          })
        );
      })
    )
  );

  declineRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(declineRequest),
      withLatestFrom(this.store.pipe(select(selectFriends))),
      switchMap(([{ friendId }, { pending }]) => {
        const friendshipId = pending.find(fs => fs.friendId === friendId)?.id as string;
        return from(this.friendsService.removeFriendship(friendshipId)).pipe(
          map(() => friendActionSuccess()),
          catchError(error => {
            console.error(error);
            return of(friendActionFailed());
          })
        );
      })
    )
  );

  removeFriendship$ = createEffect(() =>
    this.actions$.pipe(
      ofType(removeFriendship),
      withLatestFrom(this.store.pipe(select(selectFriends))),
      switchMap(([{ friendId }, { accepted }]) => {
        const friendshipId = accepted.find(fs => fs.friendId === friendId)?.id as string;
        return from(this.friendsService.removeFriendship(friendshipId)).pipe(
          map(() => friendActionSuccess()),
          catchError(error => {
            console.error(error);
            return of(friendActionFailed());
          })
        );
      })
    )
  );

  loadAcceptedFriends$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadAcceptedFriends),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      switchMap(([_, user]) =>
        this.friendsService.getAcceptedFriends(user?.id as string).pipe(
          map(accepted => loadAcceptedFriendsSuccess({ accepted })),
          catchError(error => {
            console.error(error);
            return of(friendActionFailed());
          })
        )
      )
    )
  );

  loadRequestedFriends$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadRequestedFriends),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      switchMap(([_, user]) =>
        this.friendsService.getRequestedFriends(user?.id as string).pipe(
          map(requested => loadRequestedFriendsSuccess({ requested })),
          catchError(error => {
            console.error(error);
            return of(friendActionFailed());
          })
        )
      )
    )
  );

  loadPendingFriends$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadPendingFriends),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      switchMap(([_, user]) =>
        this.friendsService.getPendingFriends(user?.id as string).pipe(
          map(pending => loadPendingFriendsSuccess({ pending })),
          catchError(error => {
            console.error(error);
            return of(friendActionFailed());
          })
        )
      )
    )
  );

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<AppState>,
    private readonly friendsService: FriendsService
  ) {}
}
