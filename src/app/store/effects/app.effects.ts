import { AuthEffects } from './auth.effects';
import { ConversationsEffects } from './conversations.effects';
import { CurrencyEffects } from './currency.effects';
import { FriendsEffects } from './friends.effects';
import { MessagesEffects } from './messages.effects';
import { RequestsEffects } from './requests.effects';
import { ReviewEffects } from './review.effects';
import { RideEffects } from './ride.effects';
import { UsersEffects } from './users.effects';

export const appEffects = [
  AuthEffects,
  RideEffects,
  FriendsEffects,
  UsersEffects,
  MessagesEffects,
  ConversationsEffects,
  CurrencyEffects,
  ReviewEffects,
  RequestsEffects,
];
