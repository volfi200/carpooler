import { Injectable } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import { LoadingController, ToastController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as firebase from 'firebase/auth';
import { catchError, EMPTY, exhaustMap, from, iif, map, of, switchMap, tap } from 'rxjs';
import { User } from '../../shared/models/User';
import { AuthService } from '../../shared/services/auth.service';
import { UserService } from '../../shared/services/user.service';
import {
  checkIfAccountExists,
  initializeUser,
  loginFailed,
  loginSuccess,
  loginWithEmail,
  loginWithFacebook,
} from '../actions/auth.actions';

@Injectable()
export class AuthEffects {
  private loading: HTMLIonLoadingElement | undefined;

  initializeUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initializeUser),
      switchMap(() => {
        if (this.auth.currentUser) {
          const { uid } = this.auth.currentUser as firebase.User;
          return this.userService.getById(uid);
        }
        return of(null);
      }),
      switchMap(user => iif(() => !!user, of(loginSuccess({ user: user as User })), EMPTY))
    )
  );

  loginWithEmailPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginWithEmail),
      tap(async () => await this.loading?.present()),
      exhaustMap(({ email, password }) =>
        from(this.authService.loginWithEmail(email, password)).pipe(
          tap(async () => await this.loading?.dismiss()),
          switchMap(() => EMPTY),
          catchError(() => of(loginFailed()))
        )
      )
    )
  );

  loginWithFacebook$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginWithFacebook),
      tap(async () => await this.loading?.present()),
      switchMap(() =>
        from(this.authService.loginWithFacebook()).pipe(
          tap(async () => await this.loading?.dismiss()),
          map(() => checkIfAccountExists()),
          catchError(() => of(loginFailed()))
        )
      )
    )
  );

  checkIfAccountExists$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(checkIfAccountExists),
        switchMap(() => {
          const { uid } = this.auth.currentUser as firebase.User;
          return this.userService.getById(uid);
        }),
        tap(storedUser => {
          if (!storedUser) {
            const { uid, email, displayName } = this.auth.currentUser as firebase.User;
            const user: User = {
              email: email as string,
              name: displayName as string,
              languages: {},
            };
            this.userService.createUser(user, uid as string);
          }
        })
      ),
    { dispatch: false }
  );

  loginFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginFailed),
        tap(async () => {
          await this.loading?.dismiss();
          const toast = await this.toastController.create({
            message: 'Login failed',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  private async initializeLoading() {
    this.loading = await this.loadingController.create({
      message: 'Loading ...',
    });
  }

  constructor(
    private readonly actions$: Actions,
    private readonly authService: AuthService,
    private readonly auth: Auth,
    private readonly userService: UserService,
    private readonly toastController: ToastController,
    private readonly loadingController: LoadingController
  ) {
    this.initializeLoading();
  }
}
