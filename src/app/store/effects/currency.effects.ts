import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, from, map, of, switchMap, tap } from 'rxjs';
import { CurrencyService } from '../../shared/services/currency.service';
import { loadCurrency, loadCurrencyFailed, loadCurrencySuccess } from '../actions/currency.actions';

@Injectable()
export class CurrencyEffects {
  loadCurrency$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadCurrency),
      switchMap(({ baseCode }) =>
        from(this.currencyService.getCurrency(baseCode)).pipe(
          map(currency => loadCurrencySuccess({ currency })),
          catchError(() => of(loadCurrencyFailed()))
        )
      )
    )
  );

  loadCurrencyFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loadCurrencyFailed),
        tap(async () => {
          const toast = await this.toastController.create({
            message: 'Loading currency has failed.',
            duration: 2000,
            buttons: [
              {
                text: 'Dismiss',
                role: 'cancel',
              },
            ],
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions,
    private readonly toastController: ToastController,
    private readonly currencyService: CurrencyService
  ) {}
}
