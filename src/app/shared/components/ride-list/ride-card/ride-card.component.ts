import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, ActionSheetOptions, NavController } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { CardAction } from '../../../../shared/models/CardAction';
import { Currency } from '../../../../shared/models/Currency';
import { flagsMap } from '../../../../shared/models/Languages';
import { Ride } from '../../../../shared/models/Ride';
import { addPassenger, deleteRide, removePassenger, setLoadedRide } from '../../../../store/actions/ride.actions';
import { AppState } from '../../../../store/models/app.state';

@Component({
  selector: 'app-ride-card',
  templateUrl: './ride-card.component.html',
  styleUrls: ['./ride-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RideCardComponent implements OnInit {
  @Input() public userId!: string;
  @Input() public ride!: Ride;
  @Input() public chosenCurrency!: Currency;
  public ownerLanguages: Array<string> = [];
  public flagsMap = flagsMap;

  constructor(
    private readonly actionSheetCtrl: ActionSheetController,
    private readonly store: Store<AppState>,
    private readonly translate: TranslateService,
    private readonly navController: NavController
  ) {}

  ngOnInit(): void {
    this.ownerLanguages = Object.keys(this.ride.owner?.languages ?? {});
  }

  public async openActionSheet(): Promise<void> {
    const options: ActionSheetOptions = {
      header: this.translate.instant('RIDE_CARD.BOOKING_OPTIONS'),
      mode: 'ios',
      buttons: [
        {
          text: this.translate.instant('RIDE_CARD.CANCEL'),
          role: 'cancel',
        },
      ],
    };
    if (this.ride.ownerId === this.userId) {
      options.buttons.push({
        text: this.translate.instant('RIDE_CARD.DELETE_RIDE'),
        role: 'destructive',
        data: {
          action: CardAction.DeleteRide,
        },
      });
    }
    if (this.ride.passengerIds.includes(this.userId)) {
      options.buttons.push({
        text: this.translate.instant('RIDE_CARD.DELETE_BOOKING'),
        role: 'destructive',
        data: {
          action: CardAction.DeleteBooking,
        },
      });
    }
    if (
      this.ride.ownerId !== this.userId &&
      !this.ride.passengerIds.includes(this.userId) &&
      this.ride.passengerIds.length < this.ride.numberOfSeats
    ) {
      options.buttons.push({
        text: this.translate.instant('RIDE_CARD.BOOK'),
        data: {
          action: CardAction.Book,
        },
      });
    }
    const actionSheet = await this.actionSheetCtrl.create(options);

    await actionSheet.present();
    const result = await actionSheet.onDidDismiss();
    this.handleResult(result);
  }

  public openRideDetails(ride: Ride): void {
    this.store.dispatch(setLoadedRide({ ride }));
    this.navController.navigateForward('home/rides/details');
  }

  private handleResult(result: OverlayEventDetail): void {
    if (result.data === undefined) {
      return;
    }
    switch (result.data?.action) {
      case CardAction.DeleteRide:
        this.store.dispatch(deleteRide({ rideId: this.ride.id as string }));
        break;
      case CardAction.DeleteBooking:
        this.store.dispatch(removePassenger({ rideId: this.ride.id as string, passengerId: this.userId }));
        break;
      case CardAction.Book:
        this.store.dispatch(addPassenger({ rideId: this.ride.id as string, passengerId: this.userId }));
        break;
      default:
        console.error('Unhandled action');
    }
  }
}
