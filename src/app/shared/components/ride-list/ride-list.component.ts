import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Currency } from '../../models/Currency';
import { Ride } from '../../models/Ride';

@Component({
  selector: 'app-ride-list',
  templateUrl: './ride-list.component.html',
  styleUrls: ['./ride-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RideListComponent implements OnInit {
  @Input() public userId!: string;
  @Input() public rides!: Ride[];
  @Input() public chosenCurrency!: Currency;

  constructor() {}

  ngOnInit() {}
}
