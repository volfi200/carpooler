import { Component, Input } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { User } from '../../../shared/models/User';
import { findConversationWith } from '../../../store/actions/conversations.actions';
import {
  acceptRequest,
  declineRequest,
  removeFriendship,
  removeInvitation,
  sendRequest,
} from '../../../store/actions/friends.actions';
import { AppState } from '../../../store/models/app.state';

@Component({
  selector: 'app-friend-tile',
  templateUrl: './friend-tile.component.html',
  styleUrls: ['./friend-tile.component.scss'],
})
export class FriendTileComponent {
  @Input() public person!: User;
  @Input() public currentUser!: User | null;
  @Input() public acceptedFriendIds: string[] = [];
  @Input() public requestedFriendIds: string[] = [];
  @Input() public pendingFriendIds: string[] = [];

  constructor(private readonly store: Store<AppState>, private readonly navController: NavController) {}

  public sendRequest(friendId: string): void {
    this.store.dispatch(sendRequest({ friendId }));
  }

  public acceptRequest(friendId: string): void {
    this.store.dispatch(acceptRequest({ friendId }));
  }

  public declineRequest(friendId: string): void {
    this.store.dispatch(declineRequest({ friendId }));
  }

  public removeInvitation(friendId: string): void {
    this.store.dispatch(removeInvitation({ friendId }));
  }

  public removeFriendship(friendId: string): void {
    this.store.dispatch(removeFriendship({ friendId }));
  }

  public openProfile(id: string): void {
    this.navController.navigateForward('home/profile', { queryParams: { id } });
  }

  public openConversation(partnerId: string): void {
    this.store.dispatch(findConversationWith({ partnerId }));
    this.navController.navigateForward('home/chats/conversation');
  }
}
