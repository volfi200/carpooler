import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { FriendTileComponent } from './components/friend-tile/friend-tile.component';
import { RideCardComponent } from './components/ride-list/ride-card/ride-card.component';
import { RideListComponent } from './components/ride-list/ride-list.component';

@NgModule({
  declarations: [RideListComponent, RideCardComponent, FriendTileComponent],
  imports: [CommonModule, IonicModule, TranslateModule],
  exports: [RideListComponent, FriendTileComponent],
})
export class SharedModule {}
