import { Message } from './Message';

export interface Conversation {
  id?: string;
  partnerName?: string;
  users: Record<string, boolean>;
  lastMessage: Message | null;
}
