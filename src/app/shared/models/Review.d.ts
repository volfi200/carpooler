import { User } from './User';

export interface Review {
  id?: string;
  date: number;
  authorId: string;
  author?: User;
  targetId: string;
  rating: number;
  content: string;
}
