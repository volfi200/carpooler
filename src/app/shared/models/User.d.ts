import { Review } from './Review';

export interface User {
  id?: string;
  email: string;
  name: string;
  languages: Record<string, boolean>;
  reviews?: Review[];
}
