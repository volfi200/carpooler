export interface Message {
  id?: string;
  conversationId?: string;
  userId: string;
  content: string;
  date: number;
}
