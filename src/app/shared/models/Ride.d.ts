import { User } from './User';

export interface Ride {
  id?: string;
  ownerId: string;
  owner?: User;
  date: string;
  passengerIds: string[];
  passengers?: User[];
  stopLocations: string[];
  numberOfSeats: number;
  currencyBaseCode: string;
  pricePerSeat: number;
  extras: Record<string, boolean>;
  isPrivate: boolean;
}
