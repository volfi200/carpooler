import { Friendship } from './Friendship';

export interface Friends {
  accepted: Friendship[];
  pending: Friendship[];
  requested: Friendship[];
}
