export interface Request {
  id?: string;
  dayInMillis: number;
  departureFrom: string;
  arrivalTo: string;
  users: Record<string, boolean>;
  userCount?: number;
}
