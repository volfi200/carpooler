import { FriendshipState } from './FriendshipState';

export interface Friendship {
  id?: string;
  friendId: string;
  state?: FriendshipState;
}
