export const languages = [
  { code: 'ENG', flag: '🇬🇧' },
  { code: 'HUN', flag: '🇭🇺' },
  { code: 'SRB', flag: '🇷🇸' },
  { code: 'SLO', flag: '🇸🇰' },
  { code: 'ROM', flag: '🇷🇴' },
  { code: 'FRA', flag: '🇫🇷' },
  { code: 'GER', flag: '🇩🇪' },
  { code: 'ITA', flag: '🇮🇹' },
  { code: 'POR', flag: '🇵🇹' },
  { code: 'RUS', flag: '🇷🇺' },
  { code: 'POL', flag: '🇵🇱' },
  { code: 'SWE', flag: '🇸🇪' },
  { code: 'DAN', flag: '🇩🇰' },
  { code: 'DUT', flag: '🇳🇱' },
  { code: 'FIN', flag: '🇫🇮' },
  { code: 'GRE', flag: '🇬🇷' },
  { code: 'ICE', flag: '🇮🇸' },
  { code: 'NOR', flag: '🇳🇴' },
  { code: 'CRO', flag: '🇭🇷' },
  { code: 'ALB', flag: '🇦🇱' },
  { code: 'BOS', flag: '🇧🇦' },
  { code: 'SPA', flag: '🇪🇸' },
  { code: 'SLV', flag: '🇸🇮' },
  { code: 'TUR', flag: '🇹🇷' },
  { code: 'CZE', flag: '🇨🇿' },
  { code: 'EST', flag: '🇪🇪' },
  { code: 'LAT', flag: '🇱🇻' },
  { code: 'LIT', flag: '🇱🇹' },
  { code: 'UKR', flag: '🇺🇦' },
  { code: 'BEL', flag: '🇧🇾' },
  { code: 'BUL', flag: '🇧🇬' },
  { code: 'MAC', flag: '🇲🇰' },
];

export const flagsMap: Record<string, string> = languages.reduce((acc: Record<string, string>, { code, flag }) => {
  acc[code] = flag;
  return acc;
}, {});
