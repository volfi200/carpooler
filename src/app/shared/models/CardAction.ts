export enum CardAction {
  DeleteRide,
  DeleteBooking,
  Book,
}
