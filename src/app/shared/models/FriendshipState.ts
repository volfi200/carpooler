export enum FriendshipState {
  PENDING = 'PENDING',
  ACCEPTED = 'ACCEPTED',
}
