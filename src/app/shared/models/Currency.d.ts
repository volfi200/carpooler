export interface Currency {
  baseCode: string;
  rates: Record<string, number>;
}
