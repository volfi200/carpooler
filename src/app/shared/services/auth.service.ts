import { Injectable } from '@angular/core';
import {
  Auth,
  createUserWithEmailAndPassword,
  signInWithCredential,
  signInWithEmailAndPassword,
} from '@angular/fire/auth';
import { FacebookLogin } from '@capacitor-community/facebook-login';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import * as firebase from 'firebase/auth';
import { initializeUser, logout } from '../../store/actions/auth.actions';
import { AppState } from '../../store/models/app.state';
import { User } from '../models/User';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private static readonly FACEBOOK_PERMISSIONS = ['email'];

  constructor(
    private readonly auth: Auth,
    private readonly navController: NavController,
    private readonly userService: UserService,
    private readonly store: Store<AppState>
  ) {
    this.auth.onAuthStateChanged(firebaseUser => {
      if (firebaseUser) {
        this.store.dispatch(initializeUser());
        this.navController.navigateRoot('/home');
      } else {
        this.navController.navigateRoot('/login');
      }
    });
  }

  public async registerWithEmail(user: User, password: string): Promise<any> {
    await createUserWithEmailAndPassword(this.auth, user.email, password)
      .then(cred => {
        this.userService.createUser(user, cred.user.uid as string);
      })
      .catch(error => {
        console.error(error);
      });
  }

  public async loginWithEmail(email: string, password: string): Promise<void> {
    await signInWithEmailAndPassword(this.auth, email, password);
  }

  public async loginWithFacebook(): Promise<void> {
    const result = await FacebookLogin.login({
      permissions: AuthService.FACEBOOK_PERMISSIONS,
    });
    if (result && result.accessToken) {
      const facebookCredential = firebase.FacebookAuthProvider.credential(result.accessToken.token);
      await signInWithCredential(this.auth, facebookCredential);
    }
  }

  public async signOut(): Promise<void> {
    this.store.dispatch(logout());
    await this.auth.signOut();
  }
}
