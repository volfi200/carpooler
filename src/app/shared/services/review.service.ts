import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  collectionData,
  CollectionReference,
  DocumentData,
  DocumentReference,
  Firestore,
  orderBy,
  query,
  where,
} from '@angular/fire/firestore';
import { distinctUntilChanged, Observable, shareReplay } from 'rxjs';
import { Review } from '../models/Review';

@Injectable({
  providedIn: 'root',
})
export class ReviewService {
  private readonly collectionName = 'Reviews';
  private readonly reviewCollection: CollectionReference<DocumentData>;

  constructor(private readonly firestore: Firestore) {
    this.reviewCollection = collection(this.firestore, this.collectionName);
  }

  public getReviewsForUserId(userId: string): Observable<Review[]> {
    const queryRef = query(this.reviewCollection, where('targetId', '==', userId), orderBy('date', 'asc'));
    return (collectionData(queryRef, { idField: 'id' }) as Observable<Review[]>).pipe(
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public createReview(review: Review): Promise<DocumentReference> {
    return addDoc(this.reviewCollection, review);
  }
}
