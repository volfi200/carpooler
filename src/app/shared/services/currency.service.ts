import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, distinctUntilChanged, EMPTY, map, Observable } from 'rxjs';
import { Currency } from '../models/Currency';

@Injectable({
  providedIn: 'root',
})
export class CurrencyService {
  private readonly endpointUrl = 'https://exchangerate-api.p.rapidapi.com/rapid/latest/';

  private readonly headers = {
    'X-RapidAPI-Key': '3b951dad63mshc13aeebf9728788p198205jsn80041a40459c',
    'X-RapidAPI-Host': 'exchangerate-api.p.rapidapi.com',
  };

  constructor(private readonly http: HttpClient) {}

  public getCurrency(base_code: string): Observable<Currency> {
    return this.http
      .get<any>(this.endpointUrl + base_code, {
        headers: this.headers,
      })
      .pipe(
        map(
          ({ rates, base_code }) =>
            <Currency>{
              baseCode: base_code,
              rates,
            }
        ),
        catchError(() => {
          console.error('Error while fetching exchange rates');
          return EMPTY;
        }),
        distinctUntilChanged()
      );
  }
}
