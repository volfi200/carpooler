import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  collectionData,
  CollectionReference,
  doc,
  DocumentData,
  DocumentReference,
  Firestore,
  limit,
  query,
  updateDoc,
  where,
} from '@angular/fire/firestore';
import { distinctUntilChanged, map, Observable, shareReplay } from 'rxjs';
import { Conversation } from '../models/Conversation';
import { Message } from '../models/Message';

@Injectable({
  providedIn: 'root',
})
export class ConversationService {
  private readonly collectionName = 'Conversations';
  private readonly conversationCollection: CollectionReference<DocumentData>;

  constructor(private readonly firestore: Firestore) {
    this.conversationCollection = collection(this.firestore, this.collectionName);
  }

  public getConversation(userId1: string, userId2: string): Observable<Conversation> {
    const queryRef = query(
      this.conversationCollection,
      where(`users.${userId1}`, '==', true),
      where(`users.${userId2}`, '==', true),
      limit(1)
    );
    return (collectionData(queryRef, { idField: 'id' }) as Observable<Conversation[]>).pipe(
      map(conversations => conversations[0]),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public getConversations(userId: string): Observable<Conversation[]> {
    const queryRef = query(this.conversationCollection, where(`users.${userId}`, '==', true));
    return (collectionData(queryRef, { idField: 'id' }) as Observable<Conversation[]>).pipe(
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public createConversation(conversation: Conversation): Promise<DocumentReference> {
    return addDoc(this.conversationCollection, conversation);
  }

  public updateLastMessage(conversationId: string, message: Message): Promise<void> {
    const conversationRef = doc(this.firestore, `${this.collectionName}/${conversationId}`);
    return updateDoc(conversationRef, {
      lastMessage: message,
    });
  }
}
