import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  collectionData,
  CollectionReference,
  deleteDoc,
  doc,
  DocumentData,
  DocumentReference,
  Firestore,
  query,
  updateDoc,
  where,
} from '@angular/fire/firestore';
import { combineLatest, distinctUntilChanged, map, Observable, shareReplay } from 'rxjs';
import { Friendship } from '../models/Friendship';
import { FriendshipState } from '../models/FriendshipState';

@Injectable({
  providedIn: 'root',
})
export class FriendsService {
  private readonly collectionName = 'Friends';
  private readonly friendsCollection: CollectionReference<DocumentData>;

  constructor(private readonly firestore: Firestore) {
    this.friendsCollection = collection(this.firestore, this.collectionName);
  }

  public getAcceptedFriends(myId: string): Observable<Friendship[]> {
    const queryRef1 = query(
      this.friendsCollection,
      where('userId1', '==', myId),
      where('state', '==', FriendshipState.ACCEPTED)
    );
    const queryRef2 = query(
      this.friendsCollection,
      where('userId2', '==', myId),
      where('state', '==', FriendshipState.ACCEPTED)
    );
    return combineLatest([
      collectionData(queryRef1, { idField: 'id' }),
      collectionData(queryRef2, { idField: 'id' }),
    ]).pipe(
      map(([array1, array2]) => [
        ...array1.map(
          friendship =>
            <Friendship>{
              id: friendship['id'],
              friendId: friendship['userId2'],
            }
        ),
        ...array2.map(
          friendship =>
            <Friendship>{
              id: friendship['id'],
              friendId: friendship['userId1'],
            }
        ),
      ]),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public getPendingFriends(myId: string): Observable<Friendship[]> {
    const queryRef = query(
      this.friendsCollection,
      where('userId2', '==', myId),
      where('state', '==', FriendshipState.PENDING)
    );
    return collectionData(queryRef, { idField: 'id' }).pipe(
      map(friendships =>
        friendships.map(
          friendship =>
            <Friendship>{
              id: friendship['id'],
              friendId: friendship['userId1'],
            }
        )
      ),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public getRequestedFriends(myId: string): Observable<Friendship[]> {
    const queryRef = query(
      this.friendsCollection,
      where('userId1', '==', myId),
      where('state', '==', FriendshipState.PENDING)
    );
    return collectionData(queryRef, { idField: 'id' }).pipe(
      map(friendships =>
        friendships.map(
          friendship =>
            <Friendship>{
              id: friendship['id'],
              friendId: friendship['userId2'],
            }
        )
      ),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public sendRequest(friendship: Friendship, myId: string): Promise<DocumentReference> {
    const { friendId, state } = friendship;
    const fs = {
      userId1: myId,
      userId2: friendId,
      state,
    };
    return addDoc(this.friendsCollection, fs);
  }

  public acceptRequest(id: string): Promise<void> {
    const friendshipRef = doc(this.firestore, `${this.collectionName}/${id}`);
    return updateDoc(friendshipRef, {
      state: FriendshipState.ACCEPTED,
    });
  }

  public removeFriendship(id: string): Promise<void> {
    const friendshipRef = doc(this.firestore, `${this.collectionName}/${id}`);
    return deleteDoc(friendshipRef);
  }
}
