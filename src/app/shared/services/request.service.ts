import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  collectionData,
  CollectionReference,
  doc,
  DocumentData,
  DocumentReference,
  Firestore,
  orderBy,
  query,
  updateDoc,
  where,
} from '@angular/fire/firestore';
import { distinctUntilChanged, Observable, shareReplay } from 'rxjs';
import { Request } from '../models/Request';

@Injectable({
  providedIn: 'root',
})
export class RequestService {
  private readonly collectionName = 'Requests';
  private readonly requestsCollection: CollectionReference<DocumentData>;

  constructor(private readonly firestore: Firestore) {
    this.requestsCollection = collection(this.firestore, this.collectionName);
  }

  public getRequests(departureFrom: string, arrivalTo: string): Observable<Request[]> {
    const todayInMillis = new Date(new Date().toISOString().slice(0, 10)).getTime();
    const queryRef = query(
      this.requestsCollection,
      where('departureFrom', '==', departureFrom),
      where('arrivalTo', '==', arrivalTo),
      where('dayInMillis', '>=', todayInMillis),
      orderBy('dayInMillis', 'asc')
    );
    return (collectionData(queryRef, { idField: 'id' }) as Observable<Request[]>).pipe(
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public getRequestForDay(departureFrom: string, arrivalTo: string, day: string): Observable<Request[]> {
    const dayInMillis = new Date(day).getTime();
    const queryRef = query(
      this.requestsCollection,
      where('departureFrom', '==', departureFrom),
      where('arrivalTo', '==', arrivalTo),
      where('dayInMillis', '==', dayInMillis)
    );
    return (collectionData(queryRef, { idField: 'id' }) as Observable<Request[]>).pipe(
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public createRequest(request: Request): Promise<DocumentReference> {
    return addDoc(this.requestsCollection, request);
  }

  public addUserToRequest(requestId: string, userId: string): Promise<void> {
    const requestRef = doc(this.firestore, `${this.collectionName}/${requestId}`);
    return updateDoc(requestRef, {
      [`users.${userId}`]: true,
    });
  }
}
