import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, distinctUntilChanged, map, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LocationService {
  private readonly endpointUrl = 'https://geodb-free-service.p.rapidapi.com/v1/geo/cities';

  private readonly headers = {
    'X-RapidAPI-Key': '3b951dad63mshc13aeebf9728788p198205jsn80041a40459c',
    'X-RapidAPI-Host': 'wft-geo-db.p.rapidapi.com',
  };

  constructor(private readonly http: HttpClient) {}

  public getFilteredCities(namePrefix: string): Observable<string[]> {
    return this.http
      .get<any>(this.endpointUrl, {
        headers: this.headers,
        params: {
          countryIds:
            'RS,HU,AT,DE,SK,CZ,PL,HR,IT,FR,ES,PT,GB,US,CA,CH,NO,SE,FI,DK,EE,LT,LV,RO,BG,GR,TR,UA,MD,ME,BA,SI,AL,RS,HR',
          namePrefix: namePrefix,
          minPopulation: 1000,
          sortDirectives: ['+population'],
          limit: 5,
          offset: 0,
        },
      })
      .pipe(
        catchError(() => {
          console.error('Error while fetching cities');
          return of([]);
        }),
        map(res => res.data.map((data: any) => data.city)),
        distinctUntilChanged()
      );
  }
}
