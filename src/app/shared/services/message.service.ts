import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  collectionData,
  CollectionReference,
  DocumentData,
  DocumentReference,
  Firestore,
  orderBy,
  query,
  where,
} from '@angular/fire/firestore';
import { distinctUntilChanged, Observable, shareReplay } from 'rxjs';
import { Message } from '../models/Message';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  private readonly collectionName = 'Messages';
  private readonly messageCollection: CollectionReference<DocumentData>;

  constructor(private readonly firestore: Firestore) {
    this.messageCollection = collection(this.firestore, this.collectionName);
  }

  public getMessages(conversationId: string): Observable<Message[]> {
    const queryRef = query(
      this.messageCollection,
      where('conversationId', '==', conversationId),
      orderBy('date', 'asc')
    );
    return (collectionData(queryRef, { idField: 'id' }) as Observable<Message[]>).pipe(
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public createMessage(message: Message): Promise<DocumentReference> {
    return addDoc(this.messageCollection, message);
  }
}
