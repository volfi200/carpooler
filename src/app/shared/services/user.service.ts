import { Injectable } from '@angular/core';
import {
  collectionData,
  doc,
  docData,
  endAt,
  Firestore,
  limit,
  orderBy,
  query,
  setDoc,
  startAt,
} from '@angular/fire/firestore';

import { collection, CollectionReference, DocumentData } from '@firebase/firestore';
import { distinctUntilChanged, Observable, shareReplay, take } from 'rxjs';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly collectionName = 'Users';
  private readonly userCollection: CollectionReference<DocumentData>;

  constructor(private readonly firestore: Firestore) {
    this.userCollection = collection(this.firestore, this.collectionName);
  }

  public createUser(user: User, id: string): Promise<void> {
    const userRef = doc(this.firestore, `${this.collectionName}/${id}`);
    return setDoc(userRef, { ...user });
  }

  public getById(id: string): Observable<User> {
    const userDocRef = doc(this.firestore, `${this.collectionName}/${id}`);
    return (docData(userDocRef, { idField: 'id' }) as Observable<User>).pipe(
      take(1),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public getByName(name: string): Observable<User[]> {
    const queryRef = query(this.userCollection, orderBy('name'), startAt(name), endAt(name + '\uf8ff'), limit(10));
    return (collectionData(queryRef, { idField: 'id' }) as Observable<User[]>).pipe(
      take(1),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }
}
