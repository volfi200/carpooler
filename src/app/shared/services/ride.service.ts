import { Injectable } from '@angular/core';
import {
  addDoc,
  arrayRemove,
  arrayUnion,
  collection,
  collectionData,
  CollectionReference,
  deleteDoc,
  doc,
  docData,
  DocumentData,
  DocumentReference,
  Firestore,
  query,
  updateDoc,
  where,
} from '@angular/fire/firestore';
import { distinctUntilChanged, Observable, shareReplay, take } from 'rxjs';
import { Ride } from '../models/Ride';

@Injectable({
  providedIn: 'root',
})
export class RideService {
  private readonly collectionName = 'Rides';
  private readonly rideCollection: CollectionReference<DocumentData>;

  constructor(private readonly firestore: Firestore) {
    this.rideCollection = collection(this.firestore, this.collectionName);
  }

  public createRide(ride: Ride): Promise<DocumentReference> {
    return addDoc(this.rideCollection, ride);
  }

  public getAll(): Observable<Ride[]> {
    return (
      collectionData(this.rideCollection, {
        idField: 'id',
      }) as Observable<Ride[]>
    ).pipe(
      take(1),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public getWhereOwner(id: string): Observable<Ride[]> {
    const queryRef = query(this.rideCollection, where('ownerId', '==', id));
    return (collectionData(queryRef, { idField: 'id' }) as Observable<Ride[]>).pipe(
      take(1),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public getWherePassenger(id: string): Observable<Ride[]> {
    const queryRef = query(this.rideCollection, where('passengerIds', 'array-contains', id));
    return (collectionData(queryRef, { idField: 'id' }) as Observable<Ride[]>).pipe(
      take(1),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public getById(id: string): Observable<Ride> {
    const userDocRef = doc(this.firestore, `${this.collectionName}/${id}`);
    return (docData(userDocRef, { idField: 'id' }) as Observable<Ride>).pipe(
      take(1),
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
      distinctUntilChanged()
    );
  }

  public addPassenger(rideId: string, passengerId: string): Promise<void> {
    const rideRef = doc(this.firestore, `${this.collectionName}/${rideId}`);
    return updateDoc(rideRef, {
      passengerIds: arrayUnion(passengerId),
    });
  }

  public removePassenger(rideId: string, passengerId: string): Promise<void> {
    const rideRef = doc(this.firestore, `${this.collectionName}/${rideId}`);
    return updateDoc(rideRef, {
      passengerIds: arrayRemove(passengerId),
    });
  }

  public deleteRide(id: string): Promise<void> {
    const rideRef = doc(this.firestore, `${this.collectionName}/${id}`);
    return deleteDoc(rideRef);
  }
}
