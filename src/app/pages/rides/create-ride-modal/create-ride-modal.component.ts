import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { AlertController, ModalController } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { catchError, debounceTime, EMPTY, iif, map, Observable, of, Subject, switchMap, take } from 'rxjs';
import { extraServices } from '../../../shared/models/ExtraServices';
import { Request } from '../../../shared/models/Request';
import { Ride } from '../../../shared/models/Ride';
import { User } from '../../../shared/models/User';
import { LocationService } from '../../../shared/services/location.service';
import { loadRequests, loadRequestsSuccess } from '../../../store/actions/requests.actions';
import { createRide } from '../../../store/actions/ride.actions';
import { AppState } from '../../../store/models/app.state';
import { selectCurrentUser } from '../../../store/selectors/auth.selector';
import { selectCurrencyBaseCode } from '../../../store/selectors/currency.selector';
import { selectRequests } from '../../../store/selectors/requests.selector';

interface HighlightedDate {
  date: string;
  textColor: string;
  backgroundColor: string;
}

@Component({
  selector: 'app-create-ride-modal',
  templateUrl: './create-ride-modal.component.html',
  styleUrls: ['./create-ride-modal.component.scss'],
})
export class CreateRideModalComponent implements OnInit {
  private inputSubject$: Subject<string> = new Subject();
  public currentUser$: Observable<User | null>;
  public chosenCurrencyBaseCode$: Observable<string>;
  public filteredCities!: Observable<string[]>;
  public requests$: Observable<Request[]>;
  public highlightedDates$: Observable<HighlightedDate[]>;
  public hasSelectedDepartureFrom = false;
  public hasSelectedArrivalTo = false;
  public rideForm: FormGroup;
  public readonly extraServices: string[] = extraServices;
  public readonly todayDate = new Date().toISOString().slice(0, 10);

  constructor(
    private readonly locationService: LocationService,
    private readonly alertController: AlertController,
    private readonly modalController: ModalController,
    private readonly formBuilder: NonNullableFormBuilder,
    private readonly store: Store<AppState>
  ) {
    this.currentUser$ = this.store.pipe(select(selectCurrentUser));
    this.chosenCurrencyBaseCode$ = this.store.pipe(select(selectCurrencyBaseCode));
    this.requests$ = this.store.pipe(select(selectRequests));
    this.rideForm = this.formBuilder.group({
      departureFrom: new FormControl('', [Validators.required]),
      arrivalTo: new FormControl('', [Validators.required]),
      date: new FormControl(new Date().toISOString(), [Validators.required]),
      numberOfSeats: new FormControl(0, [Validators.required, Validators.min(1)]),
      currencyBaseCode: new FormControl('', [Validators.required]),
      pricePerSeat: new FormControl(0, [Validators.required]),
      extras: new FormControl([]),
      isPrivate: new FormControl(false, [Validators.required]),
    });
    this.chosenCurrencyBaseCode$.pipe(take(1)).subscribe(initialValue => {
      this.rideForm.controls['currencyBaseCode'].setValue(initialValue);
    });
    this.highlightedDates$ = this.requests$.pipe(
      map(requests =>
        requests.map(
          request =>
            <HighlightedDate>{
              date: new Date(request.dayInMillis).toISOString().slice(0, 10),
              textColor: request.userCount === 1 ? '#000000' : '#ffffff',
              backgroundColor: request.userCount === 1 ? '#f7c30b' : request.userCount === 2 ? '#ea8800' : '#ea3200',
            }
        )
      )
    );
  }

  ngOnInit() {
    this.filteredCities = this.inputSubject$.pipe(
      debounceTime(1000),
      switchMap((cityNamePrefix: string) =>
        iif(
          () => !!cityNamePrefix && cityNamePrefix.length >= 2,
          this.locationService.getFilteredCities(cityNamePrefix),
          of([])
        )
      ),
      catchError(() => EMPTY)
    );
  }

  public logChange(event: any): void {
    this.inputSubject$.next(event.detail.value);
  }

  public setDepartureFromValue(cityName: string): void {
    this.hasSelectedDepartureFrom = true;
    this.rideForm.get('departureFrom')!.setValue(cityName);
    this.fetchRequests();
  }

  public unsetDepartureFrom(): void {
    this.hasSelectedDepartureFrom = false;
    this.fetchRequests();
  }

  public setArrivalToValue(cityName: string): void {
    this.hasSelectedArrivalTo = true;
    this.rideForm.get('arrivalTo')!.setValue(cityName);
    this.fetchRequests();
  }

  public unsetArrivalTo(): void {
    this.hasSelectedArrivalTo = false;
    this.fetchRequests();
  }

  public submitForm(owner: User): void {
    if (this.rideForm.valid) {
      const { departureFrom, arrivalTo, date, numberOfSeats, pricePerSeat, currencyBaseCode, extras, isPrivate } =
        this.rideForm.value;
      this.store.dispatch(
        createRide({
          ride: <Ride>{
            stopLocations: [departureFrom, arrivalTo],
            date,
            numberOfSeats,
            passengerIds: [],
            currencyBaseCode,
            pricePerSeat,
            extras: extras.reduce((acc: Record<string, boolean>, extra: string) => {
              acc[extra] = true;
              return acc;
            }, {}),
            isPrivate,
            ownerId: owner.id,
          },
          owner,
        })
      );
      this.modalController.dismiss();
    } else {
      this.presentAlert();
    }
  }

  private fetchRequests(): void {
    if (this.hasSelectedArrivalTo && this.hasSelectedDepartureFrom) {
      this.store.dispatch(
        loadRequests({
          departureFrom: this.rideForm.get('departureFrom')!.value,
          arrivalTo: this.rideForm.get('arrivalTo')!.value,
        })
      );
    } else {
      this.store.dispatch(loadRequestsSuccess({ requests: [] }));
    }
  }

  private async presentAlert(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Error',
      message: 'Please fill in all required fields.',
      buttons: ['OK'],
    });
    await alert.present();
  }
}
