import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RidesPageRoutingModule } from './rides-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { CreateRideModalComponent } from './create-ride-modal/create-ride-modal.component';
import { RidesPage } from './rides.page';

@NgModule({
  declarations: [RidesPage, CreateRideModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RidesPageRoutingModule,
    HttpClientModule,
    SharedModule,
    TranslateModule,
  ],
})
export class RidesPageModule {}
