import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { Currency } from '../../../shared/models/Currency';
import { extraServices } from '../../../shared/models/ExtraServices';
import { flagsMap } from '../../../shared/models/Languages';
import { Ride } from '../../../shared/models/Ride';
import { User } from '../../../shared/models/User';
import {
  addPassengerToLoadedRide,
  deleteLoadedRide,
  removePassengerFromLoadedRide,
} from '../../../store/actions/ride.actions';
import { AppState } from '../../../store/models/app.state';
import { selectCurrentUser } from '../../../store/selectors/auth.selector';
import { selectCurrency } from '../../../store/selectors/currency.selector';
import { selectLoadedRide } from '../../../store/selectors/rides.selector';

@Component({
  selector: 'app-ride-details',
  templateUrl: './ride-details.page.html',
  styleUrls: ['./ride-details.page.scss'],
})
export class RideDetailsPage {
  public readonly ride$: Observable<Ride | null>;
  public readonly currentUser$: Observable<User | null>;
  public readonly chosenCurrency$: Observable<Currency>;
  public ownerLanguages$: Observable<string[]>;
  public extraServices = extraServices;
  public flagsMap = flagsMap;

  constructor(private readonly store: Store<AppState>) {
    this.ride$ = this.store.pipe(select(selectLoadedRide));
    this.currentUser$ = this.store.pipe(select(selectCurrentUser));
    this.chosenCurrency$ = this.store.pipe(select(selectCurrency));
    this.ownerLanguages$ = this.ride$.pipe(map(ride => Object.keys(ride?.owner?.languages ?? {})));
  }

  public deleteRide(): void {
    this.store.dispatch(deleteLoadedRide());
  }

  public bookRide(passengerId: string): void {
    this.store.dispatch(addPassengerToLoadedRide({ passengerId }));
  }

  public deleteBooking(passengerId: string): void {
    this.store.dispatch(removePassengerFromLoadedRide({ passengerId }));
  }
}
