import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RideDetailsPageRoutingModule } from './ride-details-routing.module';

import { TranslateModule } from '@ngx-translate/core';
import { RideDetailsPage } from './ride-details.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RideDetailsPageRoutingModule, TranslateModule],
  declarations: [RideDetailsPage],
})
export class RideDetailsPageModule {}
