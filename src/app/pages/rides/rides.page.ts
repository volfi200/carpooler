import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Currency } from '../../shared/models/Currency';
import { Ride } from '../../shared/models/Ride';
import { User } from '../../shared/models/User';
import { loadRidesWhereOwner, loadRidesWherePassenger } from '../../store/actions/ride.actions';
import { AppState } from '../../store/models/app.state';
import { selectCurrentUser } from '../../store/selectors/auth.selector';
import { selectCurrency } from '../../store/selectors/currency.selector';
import { selectRides } from '../../store/selectors/rides.selector';
import { CreateRideModalComponent } from './create-ride-modal/create-ride-modal.component';

@Component({
  selector: 'app-rides',
  templateUrl: './rides.page.html',
  styleUrls: ['./rides.page.scss'],
})
export class RidesPage implements OnInit {
  public segment: 'booked' | 'offered' = 'booked';
  public readonly user$: Observable<User | null>;
  public readonly chosenCurrency$: Observable<Currency>;
  public rides$: Observable<Ride[]>;

  constructor(private readonly modalController: ModalController, private readonly store: Store<AppState>) {
    this.user$ = this.store.pipe(select(selectCurrentUser));
    this.chosenCurrency$ = this.store.pipe(select(selectCurrency));
    this.rides$ = this.store.pipe(select(selectRides));
  }

  ngOnInit(): void {
    this.reloadRides();
  }

  public reloadRides(): void {
    switch (this.segment) {
      case 'booked':
        this.store.dispatch(loadRidesWherePassenger());
        break;
      case 'offered':
        this.store.dispatch(loadRidesWhereOwner());
        break;
    }
  }

  public async openModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: CreateRideModalComponent,
      breakpoints: [0, 0.8, 1.0],
      initialBreakpoint: 1.0,
    });
    await modal.present();
  }
}
