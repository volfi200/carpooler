import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RidesPage } from './rides.page';

const routes: Routes = [
  {
    path: '',
    component: RidesPage,
  },
  {
    path: 'details',
    loadChildren: () => import('./ride-details/ride-details.module').then(m => m.RideDetailsPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RidesPageRoutingModule {}
