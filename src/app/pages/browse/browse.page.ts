import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import {
  BehaviorSubject,
  catchError,
  combineLatest,
  debounceTime,
  EMPTY,
  iif,
  map,
  Observable,
  of,
  Subject,
  switchMap,
  take,
  tap,
} from 'rxjs';
import { Currency } from '../../shared/models/Currency';
import { Request } from '../../shared/models/Request';
import { Ride } from '../../shared/models/Ride';
import { User } from '../../shared/models/User';
import { LocationService } from '../../shared/services/location.service';
import { loadAcceptedFriends } from '../../store/actions/friends.actions';
import { createRequest, loadRequestForDay, loadRequestsSuccess } from '../../store/actions/requests.actions';
import { loadAllRides } from '../../store/actions/ride.actions';
import { AppState } from '../../store/models/app.state';
import { selectCurrentUser, selectCurrentUserLanguages } from '../../store/selectors/auth.selector';
import { selectCurrency } from '../../store/selectors/currency.selector';
import { selectAcceptedFriendIds } from '../../store/selectors/friends.selector';
import { selectFirstRequest } from '../../store/selectors/requests.selector';
import { selectRides } from '../../store/selectors/rides.selector';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.page.html',
  styleUrls: ['./browse.page.scss'],
})
export class BrowsePage implements OnInit {
  private readonly inputSubject$: Subject<string> = new Subject();
  private readonly isPrivateSubject$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private readonly isCommonLanguageSubject$: BehaviorSubject<boolean> = new BehaviorSubject(true);
  public readonly todayDate = new Date().toISOString().slice(0, 10);
  public readonly rides$: Observable<Ride[]>;
  public readonly request$: Observable<Request>;
  public readonly chosenCurrency$: Observable<Currency>;
  public readonly filteredCities$: Observable<string[]>;
  public readonly hasSelectedDepartureFromSubject$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public readonly hasSelectedArrivalToSubject$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public readonly selectedDaySubject$: BehaviorSubject<string> = new BehaviorSubject(this.todayDate);
  public readonly user$: Observable<User | null>;
  public readonly userLanguages$: Observable<string[]>;
  public departureFrom: string = '';
  public arrivalTo: string = '';
  public showFilters = false;

  constructor(private readonly locationService: LocationService, private readonly store: Store<AppState>) {
    this.user$ = this.store.pipe(
      select(selectCurrentUser),
      tap(user => {
        if (user) {
          this.store.dispatch(loadAcceptedFriends());
        }
      })
    );
    this.userLanguages$ = this.store.pipe(select(selectCurrentUserLanguages)).pipe(
      tap(languages => {
        if (languages.length) {
          this.isCommonLanguageSubject$.next(true);
        } else {
          this.isCommonLanguageSubject$.next(false);
        }
      })
    );
    this.chosenCurrency$ = this.store.pipe(select(selectCurrency));
    this.request$ = this.store.pipe(select(selectFirstRequest));
    this.rides$ = combineLatest([
      this.store.pipe(select(selectRides)),
      this.hasSelectedDepartureFromSubject$,
      this.hasSelectedArrivalToSubject$,
      this.selectedDaySubject$,
      this.isPrivateSubject$,
      this.isCommonLanguageSubject$,
      this.userLanguages$,
      this.store.pipe(select(selectAcceptedFriendIds)),
    ]).pipe(
      map(
        ([
          rides,
          hasSelectedDepartureFrom,
          hasSelectedArrivalTo,
          selectedDay,
          isPrivate,
          isCommonLanguage,
          userLanguages,
          acceptedFriendIds,
        ]) => {
          let stopLocationsFilteredRides: Ride[] = [];
          if (hasSelectedDepartureFrom && hasSelectedArrivalTo) {
            stopLocationsFilteredRides = rides.filter(
              ride =>
                ride.stopLocations[0].trim() === this.departureFrom.trim() &&
                ride.stopLocations[1].trim() === this.arrivalTo.trim()
            );
          } else if (hasSelectedDepartureFrom) {
            stopLocationsFilteredRides = rides.filter(
              ride => ride.stopLocations[0].trim() === this.departureFrom.trim()
            );
          } else if (hasSelectedArrivalTo) {
            stopLocationsFilteredRides = rides.filter(ride => ride.stopLocations[1].trim() === this.arrivalTo.trim());
          } else {
            stopLocationsFilteredRides = rides;
          }

          let dateFilteredRides: Ride[] = [];
          if (selectedDay) {
            dateFilteredRides = stopLocationsFilteredRides.filter(ride => {
              return ride.date.slice(0, 10) === selectedDay;
            });
          } else {
            dateFilteredRides = stopLocationsFilteredRides;
          }

          let privateFilteredRides: Ride[] = [];
          if (isPrivate) {
            privateFilteredRides = dateFilteredRides.filter(
              ride => ride.isPrivate && acceptedFriendIds.includes(ride.ownerId)
            );
          } else {
            privateFilteredRides = dateFilteredRides.filter(
              ride => !ride.isPrivate || acceptedFriendIds.includes(ride.ownerId)
            );
          }

          let commonLanguageFilteredRides: Ride[] = [];
          if (isCommonLanguage) {
            commonLanguageFilteredRides = privateFilteredRides.filter(ride =>
              Object.keys(ride.owner?.languages ?? {}).some(key => userLanguages.includes(key))
            );
          } else {
            commonLanguageFilteredRides = privateFilteredRides;
          }
          return commonLanguageFilteredRides;
        }
      )
    );

    this.filteredCities$ = this.inputSubject$.pipe(
      debounceTime(1000),
      switchMap((cityNamePrefix: string) =>
        iif(
          () => !!cityNamePrefix && cityNamePrefix.length >= 2,
          this.locationService.getFilteredCities(cityNamePrefix),
          of([])
        )
      ),
      catchError(() => EMPTY)
    );
  }

  ngOnInit(): void {
    this.reloadRides();
  }

  public toggleShowFilters(): void {
    this.showFilters = !this.showFilters;
  }

  public reloadRides(): void {
    this.store.dispatch(loadAllRides());
  }

  public onDateChange(event: any): void {
    const dateOfDay: string = event.detail.value.slice(0, 10);
    this.selectedDaySubject$.next(dateOfDay);
    this.fetchRequests();
  }

  public onIsPrivateChange(event: any): void {
    this.isPrivateSubject$.next(event.detail.checked);
  }

  public onIsCommonLanguageChange(event: any): void {
    this.isCommonLanguageSubject$.next(event.detail.checked);
  }

  public createRequest(departureFrom: string, arrivalTo: string, date: string): void {
    this.store.dispatch(createRequest({ departureFrom, arrivalTo, date }));
  }

  public logChange(event: any): void {
    this.inputSubject$.next(event.detail.value);
  }

  public setDepartureFromValue(cityName: string): void {
    this.departureFrom = cityName;
    this.hasSelectedDepartureFromSubject$.next(true);
    this.fetchRequests();
  }

  public unsetDepartureFrom(): void {
    this.hasSelectedDepartureFromSubject$.next(false);
    this.fetchRequests();
  }

  public setArrivalToValue(cityName: string): void {
    this.arrivalTo = cityName;
    this.hasSelectedArrivalToSubject$.next(true);
    this.fetchRequests();
  }

  public unsetArrivalTo(): void {
    this.hasSelectedArrivalToSubject$.next(false);
    this.fetchRequests();
  }

  private fetchRequests(): void {
    combineLatest([this.hasSelectedDepartureFromSubject$, this.hasSelectedArrivalToSubject$, this.selectedDaySubject$])
      .pipe(take(1))
      .subscribe(([hasSelectedDepartureFrom, hasSelectedArrivalTo, selectedDay]) => {
        if (hasSelectedDepartureFrom && hasSelectedArrivalTo) {
          this.store.dispatch(
            loadRequestForDay({
              departureFrom: this.departureFrom,
              arrivalTo: this.arrivalTo,
              day: selectedDay,
            })
          );
        } else {
          this.store.dispatch(loadRequestsSuccess({ requests: [] }));
        }
      });
  }
}
