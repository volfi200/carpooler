import { Component } from '@angular/core';
import {
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  Validators,
} from '@angular/forms';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { AuthService } from '../../shared/services/auth.service';
import {
  loginWithEmail,
  loginWithFacebook,
} from '../../store/actions/auth.actions';
import { AppState } from '../../store/models/app.state';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  public loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly formBuilder: NonNullableFormBuilder,
    private navController: NavController,
    private store: Store<AppState>
  ) {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  public goToSignUp(): void {
    this.navController.navigateRoot('/sign-up');
  }

  public async loginWithEmail(): Promise<void> {
    const { email, password } = this.loginForm.value;
    this.store.dispatch(loginWithEmail({ email, password }));
  }

  public async loginWithFacebook(): Promise<void> {
    this.store.dispatch(loginWithFacebook());
  }
}
