import { Component } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { flagsMap } from '../../shared/models/Languages';
import { User } from '../../shared/models/User';
import { createReview } from '../../store/actions/review.actions';
import { loadUserById } from '../../store/actions/user.actions';
import { AppState } from '../../store/models/app.state';
import { selectCurrentUser } from '../../store/selectors/auth.selector';
import { selectLoadedUser } from '../../store/selectors/users.selector';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {
  public readonly loadedUser$: Observable<User | null>;
  public readonly currentUser$: Observable<User | null>;
  public readonly userLanguages$: Observable<string[]>;
  public readonly averageRating$: Observable<string>;
  public readonly flagsMap = flagsMap;
  public readonly reviewForm: FormGroup;

  constructor(
    private readonly store: Store<AppState>,
    private readonly route: ActivatedRoute,
    private readonly formBuilder: NonNullableFormBuilder
  ) {
    this.route.queryParams.subscribe(({ id }) => this.store.dispatch(loadUserById({ id })));
    this.loadedUser$ = this.store.pipe(select(selectLoadedUser));
    this.currentUser$ = this.store.pipe(select(selectCurrentUser));
    this.userLanguages$ = this.loadedUser$.pipe(map(user => Object.keys(user?.languages ?? {})));
    this.averageRating$ = this.loadedUser$.pipe(
      map(user =>
        (user!.reviews!.reduce((acc, review) => acc + review.rating, 0) / user!.reviews!.length).toPrecision(2)
      )
    );
    this.reviewForm = this.formBuilder.group({
      rating: new FormControl(3, [Validators.required]),
      content: new FormControl('', [Validators.required]),
    });
  }

  public submitReview(): void {
    const { rating, content } = this.reviewForm.value;
    this.store.dispatch(createReview({ rating, content }));
    this.reviewForm.reset();
  }
}
