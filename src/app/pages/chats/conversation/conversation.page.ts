import { Component, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Conversation } from '../../../shared/models/Conversation';
import { Message } from '../../../shared/models/Message';
import { User } from '../../../shared/models/User';
import { sendMessage } from '../../../store/actions/messages.actions';
import { AppState } from '../../../store/models/app.state';
import { selectCurrentUser } from '../../../store/selectors/auth.selector';
import { selectLoadedConversation } from '../../../store/selectors/conversations.selector';
import { selectMessages } from '../../../store/selectors/messages.selector';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.page.html',
  styleUrls: ['./conversation.page.scss'],
})
export class ConversationPage {
  @ViewChild(IonContent) content!: IonContent;

  public conversation$: Observable<Conversation | null>;
  public messages$: Observable<Message[]>;
  public currentUser$: Observable<User | null>;
  public newMsg = '';

  constructor(private readonly store: Store<AppState>) {
    this.conversation$ = this.store.pipe(select(selectLoadedConversation));
    this.messages$ = this.store.pipe(select(selectMessages));
    this.currentUser$ = this.store.pipe(select(selectCurrentUser));
  }

  public sendMessage(): void {
    this.store.dispatch(sendMessage({ content: this.newMsg }));
    this.newMsg = '';
    this.content.scrollToBottom();
  }
}
