import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConversationPageRoutingModule } from './conversation-routing.module';

import { TranslateModule } from '@ngx-translate/core';
import { ConversationPage } from './conversation.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ConversationPageRoutingModule, TranslateModule],
  declarations: [ConversationPage],
})
export class ConversationPageModule {}
