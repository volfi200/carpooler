import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatsPageRoutingModule } from './chats-routing.module';

import { TranslateModule } from '@ngx-translate/core';
import { ChatsPage } from './chats.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ChatsPageRoutingModule, TranslateModule],
  declarations: [ChatsPage],
})
export class ChatsPageModule {}
