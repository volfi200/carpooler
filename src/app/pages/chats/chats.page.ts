import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Conversation } from '../../shared/models/Conversation';
import { loadConversationsList, setLoadedConversation } from '../../store/actions/conversations.actions';
import { selectConversations } from '../../store/selectors/conversations.selector';
import { AppState } from '../../store/models/app.state';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {
  public conversations$: Observable<Conversation[]>;

  constructor(private readonly store: Store<AppState>, private readonly navController: NavController) {
    this.conversations$ = this.store.select(selectConversations);
  }

  ngOnInit() {
    this.store.dispatch(loadConversationsList());
  }

  public openConversation(conversation: Conversation): void {
    this.store.dispatch(setLoadedConversation({ conversation }));
    this.navController.navigateForward('home/chats/conversation');
  }
}
