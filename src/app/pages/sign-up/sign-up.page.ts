import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { languages } from '../../shared/models/Languages';
import { User } from '../../shared/models/User';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {
  private loading: HTMLIonLoadingElement | undefined;
  public signupForm: FormGroup;

  public languages = languages;

  constructor(
    private readonly formBuilder: NonNullableFormBuilder,
    public readonly loadingController: LoadingController,
    private readonly authService: AuthService
  ) {
    this.signupForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      languages: new FormControl([], [Validators.required]),
    });
  }

  async ngOnInit() {
    this.loading = await this.loadingController.create({
      message: 'Loading ...',
    });
  }

  public async signup(): Promise<void> {
    if (this.signupForm.valid) {
      const { name, email, password, languages } = this.signupForm.value;
      const langs: { [key: string]: boolean } = {};
      for (const lang of languages) {
        langs[lang] = true;
      }
      const user: User = {
        name: name as string,
        email: email as string,
        languages: langs,
      };
      await this.loading?.present();
      await this.authService.registerWithEmail(user, password);
      await this.loading?.dismiss();
      this.signupForm.reset();
    }
  }
}
