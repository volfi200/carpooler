import { Component, OnInit } from '@angular/core';
import { PickerColumnOption, PickerController } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, take } from 'rxjs';
import { Currency } from '../../shared/models/Currency';
import { User } from '../../shared/models/User';
import { AuthService } from '../../shared/services/auth.service';
import { loadCurrency } from '../../store/actions/currency.actions';
import { AppState } from '../../store/models/app.state';
import { selectCurrentUser } from '../../store/selectors/auth.selector';
import { selectCurrency } from '../../store/selectors/currency.selector';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public currency$: Observable<Currency>;
  public currentUser$: Observable<User | null>;
  public selectedLang: string;
  private displayLanguages: Record<string, string> = { en: 'English', hu: 'Magyar' };

  constructor(
    public readonly translate: TranslateService,
    private readonly authService: AuthService,
    private readonly pickerController: PickerController,
    private readonly store: Store<AppState>
  ) {
    this.currency$ = this.store.pipe(select(selectCurrency));
    this.currentUser$ = this.store.pipe(select(selectCurrentUser));
    this.selectedLang = this.displayLanguages[this.translate.currentLang];
  }

  ngOnInit(): void {
    this.currency$.pipe(take(1)).subscribe(({ baseCode }) => this.store.dispatch(loadCurrency({ baseCode })));
  }

  public readonly carpoolingPages = [
    {
      title: 'BROWSE',
      url: '/home/browse',
      icon: 'search-outline',
    },
    {
      title: 'RIDES',
      url: '/home/rides',
      icon: 'car-outline',
    },
    {
      title: 'CHATS',
      url: '/home/chats',
      icon: 'chatbubbles-outline',
    },
  ];

  public readonly accountPages = [
    {
      title: 'FRIENDS',
      url: '/home/friends',
      icon: 'people',
    },
    {
      title: 'PROFILE',
      url: '/home/profile',
      icon: 'person-circle-outline',
    },
  ];

  public async openLanguagePicker() {
    const options: PickerColumnOption[] = [];
    for (const key in this.displayLanguages) {
      options.push({
        text: this.displayLanguages[key],
        value: key,
      });
    }
    const picker = await this.pickerController.create({
      mode: 'ios',
      columns: [
        {
          name: 'languages',
          options,
        },
      ],
      buttons: [
        {
          text: this.translate.instant('PICKER.CANCEL'),
          role: 'cancel',
        },
        {
          text: this.translate.instant('PICKER.CONFIRM'),
          handler: res => {
            this.selectedLang = this.displayLanguages[res.languages.value];
            this.translate.use(res.languages.value);
          },
        },
      ],
    });
    await picker.present();
  }

  public async openCurrencyPicker(rates: Record<string, number>) {
    const options: PickerColumnOption[] = [];
    for (const key in rates) {
      options.push({
        text: key,
        value: key,
      });
    }
    const picker = await this.pickerController.create({
      mode: 'ios',
      columns: [
        {
          name: 'baseCodes',
          options,
        },
      ],
      buttons: [
        {
          text: this.translate.instant('PICKER.CANCEL'),
          role: 'cancel',
        },
        {
          text: this.translate.instant('PICKER.CONFIRM'),
          handler: result => {
            this.store.dispatch(loadCurrency({ baseCode: result.baseCodes.value }));
          },
        },
      ],
    });
    await picker.present();
  }

  public async signOut(): Promise<void> {
    await this.authService.signOut();
  }
}
