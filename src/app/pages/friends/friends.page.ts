import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Friends } from '../../shared/models/Friends';
import { User } from '../../shared/models/User';
import { loadAcceptedFriends, loadPendingFriends, loadRequestedFriends } from '../../store/actions/friends.actions';
import { loadAcceptedUsers, loadPendingUsers, loadRequestedUsers } from '../../store/actions/user.actions';
import { AppState } from '../../store/models/app.state';
import { selectCurrentUser } from '../../store/selectors/auth.selector';
import {
  selectAcceptedFriendIds,
  selectFriends,
  selectPendingFriendIds,
  selectRequestedFriendIds,
} from '../../store/selectors/friends.selector';
import { selectUsers } from '../../store/selectors/users.selector';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.page.html',
  styleUrls: ['./friends.page.scss'],
})
export class FriendsPage implements OnInit {
  public segment: 'accepted' | 'pending' | 'requested' = 'accepted';
  public readonly currentUser$: Observable<User | null>;
  public readonly users$: Observable<User[]>;
  public readonly friends$: Observable<Friends>;
  public readonly acceptedFriendIds$: Observable<string[]>;
  public readonly pendingFriendIds$: Observable<string[]>;
  public readonly requestedFriendIds$: Observable<string[]>;

  constructor(private readonly store: Store<AppState>) {
    this.currentUser$ = this.store.pipe(select(selectCurrentUser));
    this.users$ = this.store.pipe(select(selectUsers));
    this.friends$ = this.store.pipe(select(selectFriends));
    this.acceptedFriendIds$ = this.store.pipe(select(selectAcceptedFriendIds));
    this.pendingFriendIds$ = this.store.pipe(select(selectPendingFriendIds));
    this.requestedFriendIds$ = this.store.pipe(select(selectRequestedFriendIds));
  }

  ngOnInit(): void {
    this.store.dispatch(loadAcceptedFriends());
    this.store.dispatch(loadRequestedFriends());
    this.store.dispatch(loadPendingFriends());

    setTimeout(() => {
      this.store.dispatch(loadAcceptedUsers());
    }, 500);
  }

  public reloadFriends(): void {
    switch (this.segment) {
      case 'accepted':
        this.store.dispatch(loadAcceptedUsers());
        break;
      case 'pending':
        this.store.dispatch(loadPendingUsers());
        break;
      case 'requested':
        this.store.dispatch(loadRequestedUsers());
        break;
    }
  }
}
