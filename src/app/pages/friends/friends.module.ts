import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';

import { FriendsPageRoutingModule } from './friends-routing.module';

import { FriendsPage } from './friends.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, FriendsPageRoutingModule, SharedModule, TranslateModule],
  declarations: [FriendsPage],
})
export class FriendsPageModule {}
