import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from '../../../shared/models/User';
import {
  acceptRequest,
  declineRequest,
  loadAcceptedFriends,
  loadPendingFriends,
  loadRequestedFriends,
  removeFriendship,
  removeInvitation,
  sendRequest,
} from '../../../store/actions/friends.actions';
import { loadUsersBySearchTerm } from '../../../store/actions/user.actions';
import { AppState } from '../../../store/models/app.state';
import { selectCurrentUser } from '../../../store/selectors/auth.selector';
import {
  selectAcceptedFriendIds,
  selectPendingFriendIds,
  selectRequestedFriendIds,
} from '../../../store/selectors/friends.selector';
import { selectUsers } from '../../../store/selectors/users.selector';

@Component({
  selector: 'app-find-friends',
  templateUrl: './find-friends.page.html',
  styleUrls: ['./find-friends.page.scss'],
})
export class FindFriendsPage implements OnInit {
  public readonly currentUser$: Observable<User | null>;
  public readonly users$: Observable<User[]>;
  public readonly acceptedFriendIds$: Observable<string[]>;
  public readonly pendingFriendIds$: Observable<string[]>;
  public readonly requestedFriendIds$: Observable<string[]>;
  public readonly searchField: FormControl;

  constructor(private readonly store: Store<AppState>) {
    this.searchField = new FormControl('');
    this.currentUser$ = this.store.pipe(select(selectCurrentUser));
    this.users$ = this.store.pipe(select(selectUsers));
    this.acceptedFriendIds$ = this.store.pipe(select(selectAcceptedFriendIds));
    this.pendingFriendIds$ = this.store.pipe(select(selectPendingFriendIds));
    this.requestedFriendIds$ = this.store.pipe(select(selectRequestedFriendIds));
  }

  ngOnInit(): void {
    this.store.dispatch(loadAcceptedFriends());
    this.store.dispatch(loadPendingFriends());
    this.store.dispatch(loadRequestedFriends());
  }

  public searchUsers(event: any): void {
    this.store.dispatch(loadUsersBySearchTerm({ searchTerm: event.target.value }));
  }

  public sendRequest(friendId: string): void {
    this.store.dispatch(sendRequest({ friendId }));
  }

  public acceptRequest(friendId: string): void {
    this.store.dispatch(acceptRequest({ friendId }));
  }

  public declineRequest(friendId: string): void {
    this.store.dispatch(declineRequest({ friendId }));
  }

  public removeInvitation(friendId: string): void {
    this.store.dispatch(removeInvitation({ friendId }));
  }

  public removeFriendship(friendId: string): void {
    this.store.dispatch(removeFriendship({ friendId }));
  }
}
