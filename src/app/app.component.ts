import { Component } from '@angular/core';
import { FacebookLogin } from '@capacitor-community/facebook-login';
import { Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private readonly platform: Platform, public readonly translate: TranslateService) {
    this.initializeApp();
    translate.addLangs(['en', 'hu']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang() as string;
    translate.use(browserLang.match(/en|hu/) ? browserLang : 'en');
  }

  private async initializeApp(): Promise<void> {
    await this.platform.ready().then(() => {
      FacebookLogin.initialize({ appId: '718916276124501' });
    });
  }
}
