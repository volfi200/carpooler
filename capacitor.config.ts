import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.volfie.carpooler',
  appName: 'Carpooler',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
